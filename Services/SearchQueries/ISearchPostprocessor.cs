﻿using System.Collections.Generic;

namespace Services.SearchQueries
{
    public interface ISearchPostprocessor<T>
    {
        void Process(IList<T> list);
    }
}