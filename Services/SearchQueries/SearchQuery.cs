﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Services.SearchQueries
{
    internal class SearchQuery<T> : ISearchQuery<T>
    {
        private readonly IEnumerable<ISearchFilter<T>> _filters;
        private readonly IEnumerable<ISearchPostprocessor<T>> _postprocessors;

        public SearchQuery(IEnumerable<ISearchFilter<T>> filters, IEnumerable<ISearchPostprocessor<T>> postprocessors)
        {
            _filters = filters;
            _postprocessors = postprocessors;
        }

        public async Task<IEnumerable<T>> ExecuteAsync(IQueryable<T> source)
        {
            var queryResult = await _filters.Aggregate(source, (q, f) => f.Apply(q)).ToListAsync();

            foreach (var postprocessor in _postprocessors)
            {
                postprocessor.Process(queryResult);
            }

            return queryResult;
        }
    }
}