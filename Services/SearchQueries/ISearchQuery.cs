﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services.SearchQueries
{
    public interface ISearchQuery<T>
    {
        Task<IEnumerable<T>> ExecuteAsync(IQueryable<T> source);
    }
}