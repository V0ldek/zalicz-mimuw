﻿using System.Linq;

namespace Services.SearchQueries
{
    public interface ISearchFilter<T>
    {
        IQueryable<T> Apply(IQueryable<T> query);
    }
}