﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Services.Models.Course;
using Services.Models.Taskset;

namespace Services.Courses
{
    public interface ICourseService
    {
        Task<IEnumerable<CourseModel>> GetAllCoursesAsync();

        Task<IEnumerable<TasksetModel>> GetAllTasksetsForCourseAsync(string courseShortName);

        Task<bool> IsShortNameAvailableAsync(string shortName);

        Task CreateCourseAsync(CourseModel course);
    }
}