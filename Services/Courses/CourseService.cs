﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Repositories.Repositories;
using Services.Models.Course;
using Services.Models.Taskset;

namespace Services.Courses
{
    public class CourseService : ICourseService
    {
        private readonly IZaliczMimuwRepository _repository;

        public CourseService(IZaliczMimuwRepository repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<CourseModel>> GetAllCoursesAsync() =>
            (await _repository.Courses.ToListAsync()).Select(c => c.ToModel());


        public async Task<IEnumerable<TasksetModel>> GetAllTasksetsForCourseAsync(string courseShortName) =>
            (await _repository.Tasksets
                .Where(t => t.Course.ShortName == courseShortName)
                .ToListAsync())
            .Select(t => t.ToModel());

        public async Task<bool> IsShortNameAvailableAsync(string shortName) =>
            !await _repository.Courses.AnyAsync(c => c.ShortName == shortName);

        public Task CreateCourseAsync(CourseModel course)
        {
            _repository.Courses.Add(course.ToEntity());

            return _repository.SaveChangesAsync();
        }
    }
}