﻿using System.Threading.Tasks;
using Services.Models.Solution;

namespace Services.Solutions
{
    public interface ISolutionService
    {
        Task<SolutionModel> GetSolutionAsync(int solutionId);

        Task CreateSolutionAsync(CreateSolutionModel createSolutionModel);

        Task EditSolutionAsync(EditSolutionModel editSolutionModel);
    }
}