﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Repositories.Repositories;
using Services.Models.Solution;

namespace Services.Solutions
{
    internal class SolutionService : ISolutionService
    {
        private readonly IZaliczMimuwRepository _repository;

        public SolutionService(IZaliczMimuwRepository repository)
        {
            _repository = repository;
        }

        public async Task<SolutionModel> GetSolutionAsync(int solutionId) =>
            (await _repository.SolutionViews.Include(s => s.TaskView).SingleAsync(s => s.Id == solutionId)).ToModel();

        public Task CreateSolutionAsync(CreateSolutionModel createSolutionModel) =>
            _repository.CreateSolutionAsync(
                createSolutionModel.TaskId,
                createSolutionModel.Content,
                createSolutionModel.Author);

        public Task EditSolutionAsync(EditSolutionModel editSolutionModel) =>
            _repository.UpdateSolutionContentVersionAsync(
                editSolutionModel.OriginalContentVersionId,
                editSolutionModel.Content,
                editSolutionModel.Author);
    }
}