﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Repositories.Repositories;
using Services.Models.Taskset;

namespace Services.Tasksets
{
    internal class TasksetService : ITasksetService
    {
        private readonly IZaliczMimuwRepository _repository;

        public TasksetService(IZaliczMimuwRepository repository)
        {
            _repository = repository;
        }

        public async Task CreateTasksetAsync(string courseShortName, TasksetModel tasksetModel)
        {
            var entity = tasksetModel.ToEntity();
            var course = await _repository.Courses.SingleAsync(c => c.ShortName == courseShortName);

            entity.CourseId = course.Id;

            _repository.Tasksets.Add(entity);

            await _repository.SaveChangesAsync();
        }

        public async Task<bool> IsTasksetNameAndYearValidAsync(
            string courseShortName,
            string tasksetName,
            int tasksetYear) =>
            !await _repository.Tasksets.AnyAsync(
                t => t.Course.ShortName == courseShortName && t.Name == tasksetName && t.Year == tasksetYear);
    }
}