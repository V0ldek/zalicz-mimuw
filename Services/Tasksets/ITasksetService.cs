﻿using System.Threading.Tasks;
using Services.Models.Taskset;

namespace Services.Tasksets
{
    public interface ITasksetService
    {
        Task<bool> IsTasksetNameAndYearValidAsync(string courseShortName, string tasksetName, int tasksetYear);

        Task CreateTasksetAsync(string courseShortName, TasksetModel tasksetModel);
    }
}