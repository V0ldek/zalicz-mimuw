﻿namespace Services.Models.Taskset
{
    public static class TasksetExtensions
    {
        public static TasksetModel ToModel(this Repositories.Entities.Taskset taskset) =>
            new TasksetModel(taskset.Year, taskset.Name);
    }
}