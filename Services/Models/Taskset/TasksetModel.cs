﻿namespace Services.Models.Taskset
{
    public class TasksetModel
    {
        public int Year { get; internal set; }
        public string Name { get; internal set; }

        public TasksetModel(int year, string name)
        {
            Year = year;
            Name = name;
        }

        public Repositories.Entities.Taskset ToEntity() => new Repositories.Entities.Taskset {Name = Name, Year = Year};
    }
}