﻿namespace Services.Models.Task
{
    public class CreateTaskModel
    {
        public string Name { get; }
        public string Content { get; }
        public string Author { get; }
        public string CourseShortName { get; }
        public string TasksetName { get; }
        public int TasksetYear { get; }

        public CreateTaskModel(
            string name,
            string content,
            string author,
            string courseShortName,
            string tasksetName,
            int tasksetYear)
        {
            Name = name;
            Content = content;
            Author = author;
            CourseShortName = courseShortName;
            TasksetName = tasksetName;
            TasksetYear = tasksetYear;
        }
    }
}