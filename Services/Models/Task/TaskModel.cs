﻿using System;
using System.Collections.Generic;
using System.Linq;
using Repositories.Entities;
using Services.Models.Solution;

namespace Services.Models.Task
{
    public class TaskModel
    {
        public int Id { get; }
        public string Name { get; }
        public string Content { get; }
        public DateTime EditedTimestamp { get; }
        public int ContentVersionId { get; }

        public IEnumerable<SolutionModel> SolutionModels { get; }

        public TaskModel(
            int id,
            string name,
            string content,
            int contentVersionId,
            DateTime editedTimestamp,
            IEnumerable<SolutionView> solutionViews)
        {
            Id = id;
            ContentVersionId = contentVersionId;
            Name = name;
            Content = content;
            EditedTimestamp = editedTimestamp;
            SolutionModels = solutionViews.Select(s => s.ToModel());
        }
    }
}