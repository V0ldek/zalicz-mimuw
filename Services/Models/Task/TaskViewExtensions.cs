﻿using Repositories.Entities;

namespace Services.Models.Task
{
    public static class TaskViewExtensions
    {
        public static TaskModel ToModel(this TaskView taskView) => new TaskModel(
            taskView.Id,
            taskView.Name,
            taskView.Content,
            taskView.ContentVersionId,
            taskView.EditedTimestamp,
            taskView.SolutionViews);
    }
}