﻿namespace Services.Models.Task
{
    public class EditTaskModel
    {
        public string Content { get; }
        public string Author { get; }
        public int OriginalContentVersionId { get; }

        public EditTaskModel(string content, string author, int originalContentVersionId)
        {
            Content = content;
            Author = author;
            OriginalContentVersionId = originalContentVersionId;
        }
    }
}