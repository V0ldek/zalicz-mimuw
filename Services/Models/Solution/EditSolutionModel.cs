﻿namespace Services.Models.Solution
{
    public class EditSolutionModel
    {
        public string Content { get; }
        public string Author { get; }
        public int OriginalContentVersionId { get; }

        public EditSolutionModel(string content, string author, int originalContentVersionId)
        {
            Content = content;
            Author = author;
            OriginalContentVersionId = originalContentVersionId;
        }
    }
}