﻿namespace Services.Models.Solution
{
    public class CreateSolutionModel
    {
        public string Content { get; }
        public string Author { get; }
        public int TaskId { get; }

        public CreateSolutionModel(string content, string author, int taskId)
        {
            Content = content;
            Author = author;
            TaskId = taskId;
        }
    }
}