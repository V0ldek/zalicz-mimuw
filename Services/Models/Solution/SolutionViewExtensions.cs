﻿using System.Linq;
using Repositories.Entities;
using Services.Models.NegativeReview;
using Services.Models.Task;

namespace Services.Models.Solution
{
    public static class SolutionViewExtensions
    {
        public static SolutionModel ToModel(this SolutionView solutionView) => new SolutionModel(
            solutionView.Id,
            solutionView.Content,
            solutionView.EditedTimestamp,
            solutionView.UpvoteCount,
            solutionView.ContentVersionId,
            solutionView.TaskView.ToModel(),
            solutionView.NegativeReviewViews.Select(n => n.ToModel()));
    }
}