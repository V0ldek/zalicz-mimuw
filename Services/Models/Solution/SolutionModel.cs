﻿using System;
using System.Collections.Generic;
using Services.Models.NegativeReview;
using Services.Models.Task;

namespace Services.Models.Solution
{
    public class SolutionModel
    {
        public int Id { get; }
        public string Content { get; }
        public DateTime EditedTimestamp { get; }
        public int UpvoteCount { get; }
        public int ContentVersionId { get; }

        public TaskModel Task { get; }
        public IEnumerable<NegativeReviewModel> NegativeReviews { get; }

        public SolutionModel(
            int id,
            string content,
            DateTime editedTimestamp,
            int upvoteCount,
            int contentVersionId,
            TaskModel task,
            IEnumerable<NegativeReviewModel> negativeReviews)
        {
            Id = id;
            Content = content;
            EditedTimestamp = editedTimestamp;
            UpvoteCount = upvoteCount;
            ContentVersionId = contentVersionId;
            Task = task;
            NegativeReviews = negativeReviews;
        }
    }
}