﻿namespace Services.Models.NegativeReview
{
    public class EditNegativeReviewModel
    {
        public string Content { get; set; }
        public string Author { get; set; }
        public int OriginalContentVersionId { get; set; }

        public EditNegativeReviewModel(string content, string author, int originalContentVersionId)
        {
            Content = content;
            Author = author;
            OriginalContentVersionId = originalContentVersionId;
        }
    }
}