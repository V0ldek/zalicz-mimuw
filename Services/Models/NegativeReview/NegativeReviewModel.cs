﻿using System;
using Services.Models.Solution;

namespace Services.Models.NegativeReview
{
    public class NegativeReviewModel
    {
        public int Id { get; }
        public string Content { get; }
        public DateTime EditedTimestamp { get; }
        public DateTime? RejectedTimestamp { get; }
        public int ContentVersionId { get; }

        public SolutionModel Solution { get; }

        public NegativeReviewModel(
            int id,
            string content,
            DateTime editedTimestamp,
            DateTime? rejectedTimestamp,
            int contentVersionId,
            SolutionModel solution)
        {
            Id = id;
            Content = content;
            EditedTimestamp = editedTimestamp;
            RejectedTimestamp = rejectedTimestamp;
            ContentVersionId = contentVersionId;
            Solution = solution;
        }
    }
}