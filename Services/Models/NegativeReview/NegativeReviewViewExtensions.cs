﻿using Repositories.Entities;
using Services.Models.Solution;

namespace Services.Models.NegativeReview
{
    public static class NegativeReviewViewExtensions
    {
        public static NegativeReviewModel ToModel(this NegativeReviewView negativeReviewView) =>
            new NegativeReviewModel(
                negativeReviewView.Id,
                negativeReviewView.Content,
                negativeReviewView.EditedTimestamp,
                negativeReviewView.RejectedTimestamp,
                negativeReviewView.ContentVersionId,
                negativeReviewView.SolutionView.ToModel());
    }
}