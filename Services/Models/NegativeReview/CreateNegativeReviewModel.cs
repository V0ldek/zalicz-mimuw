﻿namespace Services.Models.NegativeReview
{
    public class CreateNegativeReviewModel
    {
        public string Content { get; }
        public string Author { get; }
        public int SolutionId { get; }

        public CreateNegativeReviewModel(string content, string author, int solutionId)
        {
            Content = content;
            Author = author;
            SolutionId = solutionId;
        }
    }
}