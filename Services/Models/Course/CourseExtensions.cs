﻿namespace Services.Models.Course
{
    public static class CourseExtensions
    {
        public static CourseModel ToModel(this Repositories.Entities.Course course) =>
            new CourseModel(course.ShortName, course.LongName);
    }
}