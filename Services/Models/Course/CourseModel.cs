﻿namespace Services.Models.Course
{
    public class CourseModel
    {
        public string ShortName { get; }
        public string LongName { get; }

        public CourseModel(string shortName, string longName)
        {
            ShortName = shortName;
            LongName = longName;
        }

        internal Repositories.Entities.Course ToEntity() => new Repositories.Entities.Course
            {LongName = LongName, ShortName = ShortName};
    }
}