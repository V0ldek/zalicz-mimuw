﻿using Repositories.Entities;
using Services.SearchQueries;

namespace Services.Tasks.TaskSearchQueries
{
    public interface ITaskSearchQueryBuilder
    {
        ISearchQuery<TaskView> Build();

        ITaskSearchQueryBuilder HasSolution();

        ITaskSearchQueryBuilder IncludeSolutionsAndReviews();

        ITaskSearchQueryBuilder IsInTaskset(string courseShortName, string tasksetName, int tasksetYear);

        ITaskSearchQueryBuilder OrderByName();

        ITaskSearchQueryBuilder OrderSolutionsByUpvotesDescending();

        ITaskSearchQueryBuilder OrderSolutionsNoNegativeReviewsFirst();
    }
}