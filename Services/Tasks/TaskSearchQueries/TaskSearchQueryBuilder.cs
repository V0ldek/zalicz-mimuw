﻿using System.Collections.Generic;
using Repositories.Entities;
using Services.SearchQueries;
using Services.Tasks.TaskSearchQueries.TaskSearchFilters;
using Services.Tasks.TaskSearchQueries.TaskSearchPostprocessors;

namespace Services.Tasks.TaskSearchQueries
{
    public class TaskSearchQueryBuilder : ITaskSearchQueryBuilder
    {
        private readonly List<ISearchFilter<TaskView>> _filters = new List<ISearchFilter<TaskView>>();

        private readonly List<ISearchPostprocessor<TaskView>> _postprocessors =
            new List<ISearchPostprocessor<TaskView>>();

        public ISearchQuery<TaskView> Build() => new SearchQuery<TaskView>(_filters, _postprocessors);

        public ITaskSearchQueryBuilder HasSolution()
        {
            _filters.Add(new HasSolution());
            return this;
        }

        public ITaskSearchQueryBuilder IncludeSolutionsAndReviews()
        {
            _filters.Add(new IncludeSolutionsAndReviews());
            return this;
        }

        public ITaskSearchQueryBuilder IsInTaskset(string courseShortName, string tasksetName, int tasksetYear)
        {
            _filters.Add(new IsInTaskset(courseShortName, tasksetName, tasksetYear));
            return this;
        }

        public ITaskSearchQueryBuilder OrderByName()
        {
            _filters.Add(new OrderByName());
            return this;
        }

        public ITaskSearchQueryBuilder OrderSolutionsByUpvotesDescending()
        {
            _postprocessors.Add(new OrderSolutionsByUpvotesDescending());
            return this;
        }

        public ITaskSearchQueryBuilder OrderSolutionsNoNegativeReviewsFirst()
        {
            _postprocessors.Add(new OrderSolutionsNoNegativeReviewsFirst());
            return this;
        }
    }
}