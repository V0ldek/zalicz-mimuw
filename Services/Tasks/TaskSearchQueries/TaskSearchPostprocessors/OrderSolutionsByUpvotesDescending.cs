﻿using System.Collections.Generic;
using System.Linq;
using Repositories.Entities;
using Services.SearchQueries;

namespace Services.Tasks.TaskSearchQueries.TaskSearchPostprocessors
{
    public class OrderSolutionsByUpvotesDescending : ISearchPostprocessor<TaskView>
    {
        public void Process(IList<TaskView> list)
        {
            foreach (var taskView in list)
            {
                taskView.SolutionViews = taskView.SolutionViews.OrderByDescending(s => s.UpvoteCount);
            }
        }
    }
}