﻿using System.Linq;
using Repositories.Entities;
using Services.SearchQueries;

namespace Services.Tasks.TaskSearchQueries.TaskSearchFilters
{
    public class OrderByName : ISearchFilter<TaskView>
    {
        public IQueryable<TaskView> Apply(IQueryable<TaskView> query) => query.OrderBy(t => t.Name);
    }
}