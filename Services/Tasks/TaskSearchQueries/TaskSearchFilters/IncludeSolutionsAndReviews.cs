﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Repositories.Entities;
using Services.SearchQueries;

namespace Services.Tasks.TaskSearchQueries.TaskSearchFilters
{
    public class IncludeSolutionsAndReviews : ISearchFilter<TaskView>
    {
        public IQueryable<TaskView> Apply(IQueryable<TaskView> query) =>
            query.Include(t => t.SolutionViews).ThenInclude(s => s.NegativeReviewViews);
    }
}