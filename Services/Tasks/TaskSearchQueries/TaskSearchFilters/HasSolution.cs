﻿using System.Linq;
using Repositories.Entities;
using Services.SearchQueries;

namespace Services.Tasks.TaskSearchQueries.TaskSearchFilters
{
    public class HasSolution : ISearchFilter<TaskView>
    {
        public IQueryable<TaskView> Apply(IQueryable<TaskView> query) => query.Where(t => t.SolutionViews.Any());
    }
}