﻿using System.Linq;
using Repositories.Entities;
using Services.SearchQueries;

namespace Services.Tasks.TaskSearchQueries.TaskSearchFilters
{
    public class IsInTaskset : ISearchFilter<TaskView>
    {
        private readonly string _courseShortName;
        private readonly string _tasksetName;
        private readonly int _tasksetYear;

        public IsInTaskset(string courseShortName, string tasksetName, int tasksetYear)
        {
            _courseShortName = courseShortName;
            _tasksetName = tasksetName;
            _tasksetYear = tasksetYear;
        }

        public IQueryable<TaskView> Apply(IQueryable<TaskView> query) => query.Where(
            t => t.Taskset.Course.ShortName == _courseShortName && t.Taskset.Name == _tasksetName &&
                 t.Taskset.Year == _tasksetYear);
    }
}