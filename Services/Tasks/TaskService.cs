﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Repositories.Entities;
using Repositories.Repositories;
using Services.Models.Task;
using Services.SearchQueries;
using Services.Tasks.TaskSearchQueries;
using Task = System.Threading.Tasks.Task;

namespace Services.Tasks
{
    internal class TaskService : ITaskService
    {
        private readonly IZaliczMimuwRepository _repository;

        public TaskService(IZaliczMimuwRepository repository)
        {
            _repository = repository;
        }

        public async Task<TaskModel> GetTaskAsync(int taskId) =>
            (await _repository.TaskViews.SingleAsync(t => t.Id == taskId)).ToModel();

        public ITaskSearchQueryBuilder GetSearchQueryBuilder() => new TaskSearchQueryBuilder();

        public async Task<IEnumerable<TaskModel>> GetTasksAsync(ISearchQuery<TaskView> query) =>
            (await query.ExecuteAsync(_repository.TaskViews)).Select(t => t.ToModel());

        public async Task CreateTaskAsync(CreateTaskModel createTaskModel)
        {
            var tasksetId = (await _repository.Tasksets.SingleAsync(
                t => t.Name == createTaskModel.TasksetName &&
                     t.Year == createTaskModel.TasksetYear &&
                     t.Course.ShortName == createTaskModel.CourseShortName)).Id;

            await _repository.CreateTaskAsync(
                tasksetId,
                createTaskModel.Name,
                createTaskModel.Content,
                createTaskModel.Author);
        }

        public Task EditTaskAsync(EditTaskModel editTaskModel) =>
            _repository.UpdateTaskContentVersionAsync(
                editTaskModel.OriginalContentVersionId,
                editTaskModel.Content,
                editTaskModel.Author);
    }
}