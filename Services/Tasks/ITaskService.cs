﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Repositories.Entities;
using Services.Models.Task;
using Services.SearchQueries;
using Services.Tasks.TaskSearchQueries;
using Task = System.Threading.Tasks.Task;

namespace Services.Tasks
{
    public interface ITaskService
    {
        Task<TaskModel> GetTaskAsync(int taskId);

        ITaskSearchQueryBuilder GetSearchQueryBuilder();

        Task<IEnumerable<TaskModel>> GetTasksAsync(ISearchQuery<TaskView> query);

        Task CreateTaskAsync(CreateTaskModel createTaskModel);

        Task EditTaskAsync(EditTaskModel editTaskModel);
    }
}