﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Repositories.Repositories;
using Services.Models.NegativeReview;

namespace Services.NegativeReviews
{
    internal class NegativeReviewService : INegativeReviewService
    {
        private readonly IZaliczMimuwRepository _repository;

        public NegativeReviewService(IZaliczMimuwRepository repository)
        {
            _repository = repository;
        }

        public async Task<NegativeReviewModel> GetNegativeReviewAsync(int negativeReviewId) =>
            (await _repository.NegativeReviewViews.Include(n => n.SolutionView).ThenInclude(s => s.TaskView)
                .SingleAsync(n => n.Id == negativeReviewId)).ToModel();

        public Task CreateNegativeReviewAsync(CreateNegativeReviewModel createNegativeReviewModel) =>
            _repository.CreateNegativeReviewAsync(
                createNegativeReviewModel.SolutionId,
                createNegativeReviewModel.Content,
                createNegativeReviewModel.Author);

        public Task EditNegativeReviewAsync(EditNegativeReviewModel editNegativeReviewModel) =>
            _repository.UpdateNegativeReviewContentVersionAsync(
                editNegativeReviewModel.OriginalContentVersionId,
                editNegativeReviewModel.Content,
                editNegativeReviewModel.Author);
    }
}