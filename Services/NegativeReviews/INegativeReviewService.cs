﻿using System.Threading.Tasks;
using Services.Models.NegativeReview;

namespace Services.NegativeReviews
{
    public interface INegativeReviewService
    {
        Task<NegativeReviewModel> GetNegativeReviewAsync(int negativeReviewId);

        Task CreateNegativeReviewAsync(CreateNegativeReviewModel createNegativeReviewModel);

        Task EditNegativeReviewAsync(EditNegativeReviewModel editNegativeReviewModel);
    }
}