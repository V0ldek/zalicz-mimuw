﻿using Microsoft.Extensions.DependencyInjection;
using Services.Courses;
using Services.NegativeReviews;
using Services.Solutions;
using Services.Tasks;
using Services.Tasksets;
using Services.UserFeedback;

namespace Services
{
    public static class ServiceConfigurator
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            Repositories.ServiceConfigurator.ConfigureDbContext(services);

            services.AddScoped<ICourseService, CourseService>();
            services.AddScoped<INegativeReviewService, NegativeReviewService>();
            services.AddScoped<ISolutionService, SolutionService>();
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<ITasksetService, TasksetService>();
            services.AddScoped<IUserFeedbackService, UserFeedbackService>();
        }
    }
}