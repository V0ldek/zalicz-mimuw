﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Repositories.Entities;
using Repositories.Repositories;
using Task = System.Threading.Tasks.Task;

namespace Services.UserFeedback
{
    internal class UserFeedbackService : IUserFeedbackService
    {
        private readonly IZaliczMimuwRepository _repository;

        public UserFeedbackService(IZaliczMimuwRepository repository)
        {
            _repository = repository;
        }

        public async Task<bool> CanUserUpvoteSolutionAsync(string user, int solutionId) =>
            !await _repository.Upvotes.AnyAsync(u => u.SolutionId == solutionId && u.Author == user);

        public Task UpvoteSolutionAsync(string user, int solutionId)
        {
            _repository.Upvotes.Add(new Upvote {Author = user, SolutionId = solutionId});
            return _repository.SaveChangesAsync();
        }
    }
}