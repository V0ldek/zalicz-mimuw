﻿using System.Threading.Tasks;

namespace Services.UserFeedback
{
    public interface IUserFeedbackService
    {
        Task<bool> CanUserUpvoteSolutionAsync(string user, int solutionId);

        Task UpvoteSolutionAsync(string user, int solutionId);
    }
}