# <center>Zalicz.Mimuw</center>
###### by Mateusz Gienieczko

[Zalicz.Mimuw](http://zalicz-mimuw.herokuapp.com) has one purpose – make 
studying for an upcoming test or exam on MIMUW easier. 
Let's be honest – using Google sheets and photos of solutions 
isn't the most practical way of knowledge sharing.

- You want to find tasks from past exams for practice? Select the course, 
the year that you're interested in and get the tasks right away.
- You're looking for a solution for a hard task? Find it on Zalicz.Mimuw and
see if anyone has posted a solution for it.
- You have access to tests or exams that are not on Zalicz.Mimuw yet? Simply
add a Taskset and the task's content for everyone to use.
- You know how to solve a task? Share it by adding the solution.
- Do you see an erroneous solution? Add a negative review to let everyone
now about the mistake. And if you know what's the correct solution - post it!

Zalicz.Mimuw is like Wakacje z MD – only better, and not limited to MD.

The app ~~is available~~ will soon be available at 
http://zalicz-mimuw.herokuapp.com.

This project is open-source licensed under the [MIT License](/LICENSE).

See also:
- [The VisionDoc](/Docs/VisionDoc.md).
- [Contribution guide](/CONTRIBUTING.md).
- [Changelog](/CHANGELOG).