# Contributing to Zalicz.Mimuw

The following is a guide for anyone willing to post an issue (strongly
encouraged) or to write something for the Zalicz.Mimuw project.

## Questions

If you have a question regarding the app, you might find it better to contact
me directly via email at matgienieczko@gmail.com. You can, however, post an
issue and tag it with the Question tag.

## Issues

If you found a bug in the app, have an idea for a feature that would improve
Zalicz.Mimuw or don't like our code, you can 
[post an issue via GitLab](https://gitlab.com/V0ldek/zalicz-mimuw/issues).
Be specific with your problem or feature proposal and mark it accordingly with a
Bug or Feature tag.

## Development

If you wish to contribute to the codebase, first contact me directly at
matgienieczko@gmail.com.