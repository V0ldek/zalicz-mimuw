# Zalicz.Mimuw version history and changelog.

## Version 1.0.0

### 1.0.0 Hotfix 2
- Fixed a bug where an undefined error would sometimes show during an Edit operation.

### 1.0.0 Hotfix 1
- Fixed a bug where there would be no syntax highlighting on the main Search page.
 
### Features
- Users can search for Tasks based on their Course, Taskset and whether they have submitted solutions or not.
- Users can add new Courses.
- Users can add new Tasksets for existing Courses.
- Users can add new Tasks to a Taskset.
- Users can add new Solutions to Tasks.
- Users can approve existing solutions.
- Users can add Negative Reviews to Solutions.
- Users can see the content of Tasks, Solutions and Negative Reviews with rendered Markdown, LaTeX and syntax highlighting in code snippets.
- Users can edit existing Tasks, Solutions and Negative Reviews.
