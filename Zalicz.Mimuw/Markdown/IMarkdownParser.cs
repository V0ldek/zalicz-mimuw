﻿using Microsoft.AspNetCore.Html;

namespace Zalicz.Mimuw.Markdown
{
    public interface IMarkdownParser
    {
        HtmlString ParseToHtml(string content);
    }
}