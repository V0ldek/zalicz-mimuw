﻿using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Html;
using Utilities.Assertions;

namespace Zalicz.Mimuw.Markdown
{
    internal class MarkdownParser : IMarkdownParser
    {
        public HtmlString ParseToHtml(string content) =>
            Westwind.AspNetCore.Markdown.Markdown.ParseHtmlString(
                ForceConvertDollarDigitSequences(content),
                sanitizeHtml: true);

        private static string ForceConvertDollarDigitSequences(string content)
        {
            Assert.IsNotNull(content, nameof(content));

            var divRegex = new Regex(@"\$\$[^$]+\$\$");
            var spanRegex = new Regex(@"\$[^$]+\$");

            var divPass = divRegex.Replace(
                content,
                match =>
                    "<div class=\"math\">" +
                    match.Groups[0].Value.Substring(2, match.Groups[0].Length - 4) +
                    "</div>");

            var result = spanRegex.Replace(
                divPass,
                match =>
                    "<span class=\"math\">" +
                    match.Groups[0].Value.Substring(1, match.Groups[0].Length - 2) +
                    "</span>");

            return result;
        }
    }
}