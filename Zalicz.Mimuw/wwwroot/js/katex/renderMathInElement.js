﻿function renderMathInElement(element) {
    const mathElements = element.find(".math");

    $.each(mathElements,
        function(_, mathElement) {
            const displayMode = mathElement.tagName === "div";
            const katexElement = document.createElement(mathElement.tagName);
            katexElement.setAttribute("class", displayMode ? "equation" : "inline-equation");

            try {
                katex.render(mathElement.textContent, katexElement, { displayMode: displayMode });
            } catch (err) {
                katexElement.textContent = mathElement.textContent;
            }
            mathElement.parentNode.replaceChild(katexElement, mathElement);
        });
}