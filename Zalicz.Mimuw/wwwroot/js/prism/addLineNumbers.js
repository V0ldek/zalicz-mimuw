﻿function addLineNumbers(root) {
    const codeElements = root.find("code");

    $.each(codeElements,
        function(_, element) {
            $(element).addClass("line-numbers");
        });
}