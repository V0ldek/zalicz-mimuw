﻿function setupAjaxErrorHandler() {
    $.ajaxSetup({
        error: function (xhr, status, error) {
            if (status === "error") {
                loadErrorPage(xhr.responseText);
            }
        }
    });
}