﻿function renderMarkdown(element, content) {
    $.ajax({
        type: "POST",
        url: "/Markdown/Parse",
        dataType: "text",
        data: { content: content },
        success: function(response) {
            element.html(response);
            renderMathInElement(element);
            addLineNumbers(element);
            Prism.highlightAllUnder(element[0]);
        }
    });

    return false;
}