﻿var autoRefreshTimeouts = {};

function resetTimer(key, timeout, handler) {
    clearTimeout(autoRefreshTimeouts[key]);
    autoRefreshTimeouts[key] = setTimeout(handler, timeout);
}

function addAutoRefreshOnIdle(element, timeout, handler) {
    var id = element.attr("id");
    element.keydown(function() {
        resetTimer(id, timeout, handler);
    });
}