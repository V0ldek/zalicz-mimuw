﻿function setPreview(previewElement, sourceElement) {
    function getVal() {
        return $(sourceElement).val() ? $(sourceElement).val() : $(sourceElement).attr("placeholder");
    }

    addAutoRefreshOnIdle(sourceElement,
        500,
        function() {
            renderMarkdown(previewElement, getVal());
        });

    renderMarkdown(previewElement, getVal());
}