﻿function renderErrorResponse(response, resultsElement) {
    resultsElement.html(response.find("#error-body"));
    resultsElement.attr("class", resultsElement.attr("class") + " alert-danger border-danger");
    resultsElement.css("border", "3px solid");
    resultsElement.css("padding", "5px");
}