﻿function populateTasksetList(courseSelect, tasksetSelect) {
    tasksetSelect.find("option").remove();

    $.ajax({
        global: false,  // Do not trigger the global ajaxStart and ajaxStop (suppresses the loader in Search).
        type: "GET",
        url: "/Taskset/CourseTasksetsSelectListItems",
        data: `courseShortName=${courseSelect.val()}`,
        dataType: "json",
        success: function(response) {
            response.forEach(function(item) {
                tasksetSelect.append($("<option/>", item));
            });
        }
    });
}