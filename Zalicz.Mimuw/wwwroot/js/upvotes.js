﻿function incrementUpvotesHtml(solutionId) {
    const upvotes = $(`#solution-${solutionId}-upvote-count`);
    var counter = parseInt(upvotes.text());
    ++counter;
    upvotes.text(counter);
}

function submitUpvote(solutionId) {
    $.ajax({
        global: false,  // Do not trigger the global ajaxStart and ajaxStop (suppresses the loader in Search).
        method: "POST",
        url: "/UserFeedback/UpvoteSolution",
        data: { solutionId: solutionId },
        error: function(xhr) {
            console.info(xhr);
            if (xhr.status === 400 || xhr.status === 403) {
                const alert = $(`#upvote-error-alert-${solutionId}`);
                alert.text(xhr.responseText);
                alert.show();
                alert.fadeOut(4000);
            } else {
                loadErrorPage(xhr.responseText);
            }
        },
        success: function() {
            incrementUpvotesHtml(solutionId);
        }
    });

    return false;
}