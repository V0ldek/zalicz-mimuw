﻿using Microsoft.AspNetCore.Mvc;
using Zalicz.Mimuw.Models.NegativeReview;

namespace Zalicz.Mimuw.Views.Shared.Components.NegativeReview
{
    public class NegativeReviewViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(NegativeReviewViewModel negativeReviewViewModel) =>
            View("NegativeReviewCard", negativeReviewViewModel);
    }
}