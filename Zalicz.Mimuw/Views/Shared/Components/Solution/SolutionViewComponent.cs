﻿using Microsoft.AspNetCore.Mvc;
using Zalicz.Mimuw.Models.Solution;

namespace Zalicz.Mimuw.Views.Shared.Components.Solution
{
    public class SolutionViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(SolutionViewModel solutionViewModel) =>
            View("SolutionCard", solutionViewModel);
    }
}