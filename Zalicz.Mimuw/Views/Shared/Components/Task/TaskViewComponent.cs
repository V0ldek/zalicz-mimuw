﻿using Microsoft.AspNetCore.Mvc;
using Zalicz.Mimuw.Models.Task;

namespace Zalicz.Mimuw.Views.Shared.Components.Task
{
    public class TaskViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(TaskViewModel taskViewModel) => View("TaskCard", taskViewModel);
    }
}