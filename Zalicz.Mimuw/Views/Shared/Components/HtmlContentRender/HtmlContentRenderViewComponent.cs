﻿using Microsoft.AspNetCore.Mvc;
using Zalicz.Mimuw.Markdown;

namespace Zalicz.Mimuw.Views.Shared.Components.HtmlContentRender
{
    public class HtmlContentRenderViewComponent : ViewComponent
    {
        private readonly IMarkdownParser _markdownParser;

        public HtmlContentRenderViewComponent(IMarkdownParser markdownParser)
        {
            _markdownParser = markdownParser;
        }

        public IViewComponentResult Invoke(string content)
        {
            var htmlContent = _markdownParser.ParseToHtml(content);

            return View("Content", htmlContent);
        }
    }
}