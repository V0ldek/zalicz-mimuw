﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Zalicz.Mimuw.Models.Task;

namespace Zalicz.Mimuw.Views.Search.Components.SearchResult
{
    public class SearchResultViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(ICollection<TaskViewModel> taskViewModels) =>
            taskViewModels == null || !taskViewModels.Any()
                ? View("Empty", taskViewModels)
                : View("Results", taskViewModels);
    }
}