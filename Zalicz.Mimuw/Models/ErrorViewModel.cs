using System.Net;

namespace Zalicz.Mimuw.Models
{
    public class ErrorViewModel
    {
        public HttpStatusCode StatusCode { get; }
        public string Message { get; }

        public ErrorViewModel(HttpStatusCode statusCode, string message)
        {
            StatusCode = statusCode;
            Message = message;
        }
    }
}