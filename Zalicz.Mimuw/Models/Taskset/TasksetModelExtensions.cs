﻿using Services.Models.Taskset;

namespace Zalicz.Mimuw.Models.Taskset
{
    public static class TasksetModelExtensions
    {
        public static TasksetViewModel ToViewModel(this TasksetModel tasksetModel) =>
            new TasksetViewModel(tasksetModel.Year, tasksetModel.Name);
    }
}