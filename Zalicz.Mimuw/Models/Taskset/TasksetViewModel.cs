﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace Zalicz.Mimuw.Models.Taskset
{
    public class TasksetViewModel
    {
        public int Year { get; }
        public string Name { get; }

        public string Identifier => $"{Name}@{Year}";

        public TasksetViewModel(int year, string name)
        {
            Year = year;
            Name = name;
        }

        public static (string name, int year) DeconstructIdentifier(string identifier)
        {
            var splitNameAndYear = identifier.Split('@');
            var tasksetName = splitNameAndYear[0];
            var tasksetYear = int.Parse(splitNameAndYear[1]);

            return (tasksetName, tasksetYear);
        }

        public override string ToString() => $"{Name} - {Year}";

        public SelectListItem ToSelectListItem() => new SelectListItem {Value = Identifier, Text = ToString()};
    }
}