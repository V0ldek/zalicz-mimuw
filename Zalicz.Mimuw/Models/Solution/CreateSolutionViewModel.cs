﻿using System.ComponentModel.DataAnnotations;
using Services.Models.Solution;
using Utilities.Assertions;
using Zalicz.Mimuw.Models.Task;

namespace Zalicz.Mimuw.Models.Solution
{
    public class CreateSolutionViewModel
    {
        public int TaskId { get; set; }

        [Display(Name = "Task")]
        public string TaskContent { get; set; }

        [Display(Name = "Content")]
        [Required(ErrorMessage = "Solution content is required.")]
        public string Content { get; set; }

        public string Author { get; set; }

        public CreateSolutionViewModel()
        {
        }

        public CreateSolutionViewModel(TaskViewModel taskViewModel)
        {
            TaskId = taskViewModel.Id;
            TaskContent = taskViewModel.Content;
        }

        public CreateSolutionModel ToModel()
        {
            Assert.IsNotZero(TaskId, $"{nameof(TaskId)} was not set before converting to model.");
            Assert.IsNotNullOrEmpty(Author, $"{nameof(Author)} was not set before converting to model.");

            return new CreateSolutionModel(Content, Author, TaskId);
        }
    }
}