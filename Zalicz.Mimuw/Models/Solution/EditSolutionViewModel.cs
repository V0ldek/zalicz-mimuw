﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Services.Models.Solution;
using Utilities.Assertions;

namespace Zalicz.Mimuw.Models.Solution
{
    public class EditSolutionViewModel
    {
        [Display(Name = "Content")]
        [Required(ErrorMessage = "Solution content is required.")]
        [Remote(
            "IsContentUpdateValid",
            "Solution",
            HttpMethod = "Post",
            AdditionalFields = nameof(OriginalContent))]
        public string Content { get; set; }

        public string Author { get; set; }

        public int OriginalContentVersionId { get; set; }

        [Display(Name = "Original solution content")]
        public string OriginalContent { get; set; }

        [Display(Name = "Task")]
        public string TaskContent { get; set; }

        public EditSolutionModel ToModel()
        {
            Assert.IsNotZero(
                OriginalContentVersionId,
                $"{nameof(OriginalContentVersionId)} was not set before converting to model.");
            Assert.IsNotNullOrEmpty(Author, $"{nameof(Author)} was not set before converting to model.");

            return new EditSolutionModel(Content, Author, OriginalContentVersionId);
        }
    }
}