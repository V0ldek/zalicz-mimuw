﻿using Services.Models.Solution;

namespace Zalicz.Mimuw.Models.Solution
{
    public static class SolutionModelExtensions
    {
        public static SolutionViewModel ToViewModel(this SolutionModel solutionModel) =>
            new SolutionViewModel(
                solutionModel.Id,
                solutionModel.Content,
                solutionModel.EditedTimestamp,
                solutionModel.UpvoteCount,
                solutionModel.Task.Id,
                solutionModel.NegativeReviews);

        public static EditSolutionViewModel ToEditViewModel(this SolutionModel solutionModel) =>
            new EditSolutionViewModel
            {
                OriginalContentVersionId = solutionModel.ContentVersionId,
                OriginalContent = solutionModel.Content,
                TaskContent = solutionModel.Task.Content,
                Content = solutionModel.Content
            };
    }
}