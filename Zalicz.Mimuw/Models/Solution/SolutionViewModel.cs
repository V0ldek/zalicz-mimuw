﻿using System;
using System.Collections.Generic;
using System.Linq;
using Services.Models.NegativeReview;
using Zalicz.Mimuw.Models.NegativeReview;

namespace Zalicz.Mimuw.Models.Solution
{
    public class SolutionViewModel
    {
        public int Id { get; }
        public string Content { get; }
        public DateTime EditedTimestamp { get; }
        public int Upvotes { get; }
        public bool HasNotRejectedNegativeReviews { get; }
        public int TaskId { get; }

        public IEnumerable<NegativeReviewViewModel> NegativeReviews { get; }

        public SolutionViewModel(
            int id,
            string content,
            DateTime editedTimestamp,
            int upvotes,
            int taskId,
            IEnumerable<NegativeReviewModel> negativeReviews)
        {
            Id = id;
            Content = content;
            EditedTimestamp = editedTimestamp;
            Upvotes = upvotes;
            TaskId = taskId;
            NegativeReviews = negativeReviews.Select(n => n.ToViewModel()).ToList();
            HasNotRejectedNegativeReviews = NegativeReviews.Any(n => !n.IsRejected);
        }
    }
}