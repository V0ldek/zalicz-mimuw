﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services.Models.Task;
using Utilities.Assertions;
using Zalicz.Mimuw.Models.Course;
using Zalicz.Mimuw.Models.Taskset;

namespace Zalicz.Mimuw.Models.Task
{
    public class CreateTaskViewModel
    {
        [Display(Name = "Course")]
        public string CourseValue { get; set; }

        [Display(Name = "Taskset")]
        public string TasksetValue { get; set; }

        [Display(Name = "Name")]
        [RegularExpression(
            @"[A-Za-zĄ-Żą-ż0-9 \-\:]*",
            ErrorMessage =
                "Name may contain only letters, numbers, -, : and space characters.")]
        [MaxLength(32, ErrorMessage = "Name cannot be longer than 32 characters.")]
        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }

        [Display(Name = "Content")]
        [Required(ErrorMessage = "Task content is required.")]
        public string Content { get; set; }

        internal string Author { get; set; }

        public List<SelectListItem> Courses { get; set; }

        public CreateTaskViewModel()
        {
        }

        public CreateTaskViewModel(IEnumerable<CourseViewModel> courses)
        {
            Courses = courses.Select(c => c.ToSelectListItem()).ToList();
        }

        public CreateTaskModel ToModel()
        {
            Assert.IsNotNullOrEmpty(Author, $"{nameof(Author)} was not set before converting to model.");

            var (tasksetName, tasksetYear) = TasksetViewModel.DeconstructIdentifier(TasksetValue);

            return new CreateTaskModel(Name, Content, Author, CourseValue, tasksetName, tasksetYear);
        }
    }
}