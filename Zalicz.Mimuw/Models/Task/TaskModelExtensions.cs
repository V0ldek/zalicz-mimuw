﻿using Services.Models.Task;

namespace Zalicz.Mimuw.Models.Task
{
    public static class TaskModelExtensions
    {
        public static TaskViewModel ToViewModel(this TaskModel taskModel) =>
            new TaskViewModel(
                taskModel.Id,
                taskModel.Name,
                taskModel.Content,
                taskModel.EditedTimestamp,
                taskModel.SolutionModels,
                taskModel.ContentVersionId);

        public static EditTaskViewModel ToEditViewModel(this TaskModel taskModel) =>
            new EditTaskViewModel
            {
                Name = taskModel.Name,
                OriginalContentVersionId = taskModel.ContentVersionId,
                OriginalContent = taskModel.Content,
                Content = taskModel.Content
            };
    }
}