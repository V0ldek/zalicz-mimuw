﻿using System;
using System.Collections.Generic;
using System.Linq;
using Services.Models.Solution;
using Zalicz.Mimuw.Models.Solution;

namespace Zalicz.Mimuw.Models.Task
{
    public class TaskViewModel
    {
        public int Id { get; }
        public string Name { get; }
        public string Content { get; }
        public DateTime EditedTimestamp { get; }
        public int ContentVersionId { get; }

        public IEnumerable<SolutionViewModel> Solutions { get; }

        public TaskViewModel(
            int id,
            string name,
            string content,
            DateTime editedTimestamp,
            IEnumerable<SolutionModel> solutions,
            int contentVersionId)
        {
            Id = id;
            Name = name;
            Content = content;
            EditedTimestamp = editedTimestamp;
            ContentVersionId = contentVersionId;
            Solutions = solutions.Select(s => s.ToViewModel()).ToList();
        }
    }
}