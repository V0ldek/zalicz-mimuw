﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Services.Models.Task;
using Utilities.Assertions;

namespace Zalicz.Mimuw.Models.Task
{
    public class EditTaskViewModel
    {
        [Display(Name = "Name")]
        public string Name { get; set; }

        public int OriginalContentVersionId { get; set; }

        [Display(Name = "Original task content")]
        public string OriginalContent { get; set; }

        [Display(Name = "Content")]
        [Required(ErrorMessage = "Task content is required.")]
        [Remote(
            "IsContentUpdateValid",
            "Task",
            HttpMethod = "Post",
            AdditionalFields = nameof(OriginalContent))]
        public string Content { get; set; }

        public string Author { get; set; }

        public EditTaskModel ToModel()
        {
            Assert.IsNotZero(
                OriginalContentVersionId,
                $"{nameof(OriginalContentVersionId)} was not set before converting to model.");
            Assert.IsNotNullOrEmpty(Author, $"{nameof(Author)} was not set before converting to model.");

            return new EditTaskModel(Content, Author, OriginalContentVersionId);
        }
    }
}