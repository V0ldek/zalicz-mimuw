﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Services.Models.NegativeReview;
using Utilities.Assertions;

namespace Zalicz.Mimuw.Models.NegativeReview
{
    public class EditNegativeReviewViewModel
    {
        [Display(Name = "New content")]
        [Required(ErrorMessage = "Content is required.")]
        [Remote(
            "IsContentUpdateValid",
            "NegativeReview",
            HttpMethod = "Post",
            AdditionalFields = nameof(OriginalContent))]
        public string Content { get; set; }

        public string Author { get; set; }

        public int OriginalContentVersionId { get; set; }

        [Display(Name = "Original review content")]
        public string OriginalContent { get; set; }

        [Display(Name = "Task")]
        public string TaskContent { get; set; }

        [Display(Name = "Solution")]
        public string SolutionContent { get; set; }

        public EditNegativeReviewModel ToModel()
        {
            Assert.IsNotZero(
                OriginalContentVersionId,
                $"{nameof(OriginalContentVersionId)} was not set before converting to model.");
            Assert.IsNotNullOrEmpty(Author, $"{nameof(Author)} was not set before converting to model.");

            return new EditNegativeReviewModel(Content, Author, OriginalContentVersionId);
        }
    }
}