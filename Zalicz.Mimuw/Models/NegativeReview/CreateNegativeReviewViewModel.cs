﻿using System.ComponentModel.DataAnnotations;
using Services.Models.NegativeReview;
using Services.Models.Solution;
using Utilities.Assertions;

namespace Zalicz.Mimuw.Models.NegativeReview
{
    public class CreateNegativeReviewViewModel
    {
        [Display(Name = "Content")]
        [Required(ErrorMessage = "Review content is required.")]
        public string Content { get; set; }

        public string Author { get; set; }

        public int SolutionId { get; set; }

        [Display(Name = "Solution under review")]
        public string SolutionContent { get; set; }

        [Display(Name = "Task")]
        public string TaskContent { get; set; }

        public CreateNegativeReviewViewModel()
        {
        }

        public CreateNegativeReviewViewModel(SolutionModel solution)
        {
            SolutionId = solution.Id;
            SolutionContent = solution.Content;
            TaskContent = solution.Task.Content;
        }

        public CreateNegativeReviewModel ToModel()
        {
            Assert.IsNotZero(SolutionId, $"{nameof(SolutionId)} was not set before converting to model.");
            Assert.IsNotNullOrEmpty(Author, $"{nameof(Author)} was not set before converting to model.");

            return new CreateNegativeReviewModel(Content, Author, SolutionId);
        }
    }
}