﻿using System;

namespace Zalicz.Mimuw.Models.NegativeReview
{
    public class NegativeReviewViewModel
    {
        public int Id { get; }
        public string Content { get; }
        public DateTime EditedTimestamp { get; }
        public DateTime? RejectedTimestamp { get; }
        public int TaskId { get; }
        public int SolutionId { get; }

        public bool IsRejected => RejectedTimestamp != null;

        public NegativeReviewViewModel(
            int id,
            string content,
            DateTime editedTimestamp,
            DateTime? rejectedTimestamp,
            int taskId,
            int solutionId)
        {
            Id = id;
            Content = content;
            EditedTimestamp = editedTimestamp;
            RejectedTimestamp = rejectedTimestamp;
            TaskId = taskId;
            SolutionId = solutionId;
        }
    }
}