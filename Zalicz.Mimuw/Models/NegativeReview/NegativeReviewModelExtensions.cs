﻿using Services.Models.NegativeReview;

namespace Zalicz.Mimuw.Models.NegativeReview
{
    public static class NegativeReviewModelExtensions
    {
        public static NegativeReviewViewModel ToViewModel(this NegativeReviewModel negativeReviewModel) =>
            new NegativeReviewViewModel(
                negativeReviewModel.Id,
                negativeReviewModel.Content,
                negativeReviewModel.EditedTimestamp,
                negativeReviewModel.RejectedTimestamp,
                negativeReviewModel.Solution.Task.Id,
                negativeReviewModel.Solution.Id);

        public static EditNegativeReviewViewModel ToEditViewModel(this NegativeReviewModel negativeReviewModel) =>
            new EditNegativeReviewViewModel
            {
                OriginalContentVersionId = negativeReviewModel.ContentVersionId,
                OriginalContent = negativeReviewModel.Content,
                TaskContent = negativeReviewModel.Solution.Task.Content,
                SolutionContent = negativeReviewModel.Solution.Content,
                Content = negativeReviewModel.Content
            };
    }
}