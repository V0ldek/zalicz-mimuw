﻿using Services.Models.Course;

namespace Zalicz.Mimuw.Models.Course
{
    public static class CourseModelExtensions
    {
        public static CourseViewModel ToViewModel(this CourseModel courseModel) =>
            new CourseViewModel(courseModel.ShortName, courseModel.LongName);
    }
}