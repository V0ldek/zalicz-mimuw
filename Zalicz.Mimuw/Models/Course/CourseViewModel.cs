﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace Zalicz.Mimuw.Models.Course
{
    public class CourseViewModel
    {
        public string ShortName { get; }
        public string LongName { get; }

        public CourseViewModel(string shortName, string longName)
        {
            ShortName = shortName;
            LongName = longName;
        }

        public override string ToString() => $"[{ShortName}] {LongName}";

        public SelectListItem ToSelectListItem() => new SelectListItem {Value = ShortName, Text = ToString()};
    }
}