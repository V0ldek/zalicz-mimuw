﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Services.Models.Course;

namespace Zalicz.Mimuw.Models.Course
{
    public class CreateCourseViewModel
    {
        [Display(Name = "Short name")]
        [RegularExpression(
            "[A-Za-z0-9]*",
            ErrorMessage = "The short name must be a sequence of letters and/or numbers.")]
        [MinLength(2, ErrorMessage = "Short name must be at least 2 characters long.")]
        [MaxLength(8, ErrorMessage = "Short name cannot be longer than 8 characters.")]
        [Required(ErrorMessage = "Short name is required.")]
        [Remote(
            "IsShortNameAvailable",
            "Course")]
        public string ShortName { get; set; }

        [Display(Name = "Long name")]
        [RegularExpression(
            @"[A-Za-zĄ-Żą-ż0-9 \-\:]*",
            ErrorMessage =
                "The long name may contain only letters, numbers, -, : and space characters.")]
        [MinLength(6, ErrorMessage = "Long name must be at least 6 characters long.")]
        [MaxLength(64, ErrorMessage = "Long name cannot be longer than 64 characters.")]
        [Required(ErrorMessage = "Long name is required.")]
        public string LongName { get; set; }

        // This property is autogenerated and displayed on the front-end, has no use server-side.
        [Display(Name = "Full name")]
        public string FullName { get; set; }

        public CourseModel ToModel() => new CourseModel(ShortName, LongName);
    }
}