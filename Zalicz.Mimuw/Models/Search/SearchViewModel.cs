﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zalicz.Mimuw.Models.Course;

namespace Zalicz.Mimuw.Models.Search
{
    public class SearchViewModel
    {
        [Display(Name = "Course")]
        [Required(ErrorMessage = "Select a course.")]
        public string CourseShortName { get; set; }

        [Display(Name = "Taskset")]
        [Required(ErrorMessage = "Select a taskset.")]
        public string TasksetNameAndYear { get; set; }

        [Display(Name = "Include only tasks with solutions")]
        public bool RequireSolution { get; set; }

        public List<SelectListItem> Courses { get; set; }

        public SearchViewModel()
        {
        }

        public SearchViewModel(IEnumerable<CourseViewModel> courses)
        {
            Courses = courses.Select(c => c.ToSelectListItem()).ToList();
        }
    }
}