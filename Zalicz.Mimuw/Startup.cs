﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Markdig;
using Markdig.Extensions.EmphasisExtras;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services;
using Westwind.AspNetCore.Markdown;
using Zalicz.Mimuw.Cookies;
using Zalicz.Mimuw.Markdown;
using IMarkdownParser = Westwind.AspNetCore.Markdown.IMarkdownParser;

namespace Zalicz.Mimuw
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(
                options =>
                {
                    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                    options.CheckConsentNeeded = context => true;
                    options.MinimumSameSitePolicy = SameSiteMode.None;
                });

            services.AddLocalization(
                options => { options.ResourcesPath = "Resources"; });

            services.AddMarkdown(
                config =>
                {
                    config.ConfigureMarkdigPipeline = builder =>
                    {
                        builder.UseEmphasisExtras(EmphasisExtraOptions.Strikethrough)
                            .UseMathematics()
                            .UsePipeTables()
                            .UseGridTables()
                            .UseAutoLinks() // URLs are parsed into anchors
                            .UseAbbreviations()
                            .UseYamlFrontMatter()
                            .UseEmojiAndSmiley()
                            .UseListExtras()
                            .UseFigures()
                            .UseTaskLists()
                            .UseCustomContainers();
                    };
                });

            services.AddMvc()
                .AddDataAnnotationsLocalization()
                .AddViewLocalization()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddScoped<ICookieProvider<UserIdCookie>, UserIdCookieProvider>();
            services.AddScoped<Markdown.IMarkdownParser, MarkdownParser>();
            ServiceConfigurator.ConfigureServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        [SuppressMessage(
            "Sonar Smell",
            "S1075: Refactor your code not to use hardcoded absolute paths or URIs",
            Justification = "Error handler has to be set somewhere.")]
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();

            var supportedCultures = new List<CultureInfo>
            {
                new CultureInfo("en-GB"),
                new CultureInfo("pl-PL")
            };
            app.UseRequestLocalization(
                new RequestLocalizationOptions
                {
                    DefaultRequestCulture = new RequestCulture("pl-PL"),
                    SupportedCultures = supportedCultures,
                    SupportedUICultures = supportedCultures
                });

            app.UseMvc(
                routes =>
                {
                    routes.MapRoute(
                        "default",
                        "{controller=Home}/{action=Search}/{id?}");
                });

            app.UseMarkdown();
        }
    }
}