﻿using System;
using System.Runtime.Serialization;

namespace Zalicz.Mimuw.Cookies
{
    [Serializable]
    public class CookieConsentException : ApplicationException
    {
        public CookieConsentException(string message) : base(message)
        {
        }

        protected CookieConsentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}