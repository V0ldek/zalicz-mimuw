﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;

namespace Zalicz.Mimuw.Cookies
{
    public class UserIdCookieProvider : CookieProviderBase<UserIdCookie>
    {
        protected override string Key => UserIdCookie.Key;

        protected override CookieOptions Options => new CookieOptions
        {
            Expires = DateTimeOffset.MaxValue,
            HttpOnly = true
        };

        public UserIdCookieProvider(
            IHttpContextAccessor httpContextAccessor,
            IStringLocalizer<SharedResources> sharedLocalizer) : base(httpContextAccessor, sharedLocalizer)
        {
        }

        protected override UserIdCookie FromValue(string value) => UserIdCookie.FromValue(value);
    }
}