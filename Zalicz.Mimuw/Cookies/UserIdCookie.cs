﻿using System;

namespace Zalicz.Mimuw.Cookies
{
    public class UserIdCookie
    {
        public const string Key = "zalicz-mimuw.UserId";
        public Guid UserId { get; } = Guid.NewGuid();

        // IntelliSense says it's never used, but it is in the CookieProviderBase (T has a new() constraint).
        public UserIdCookie()
        {
        }

        private UserIdCookie(Guid userId)
        {
            UserId = userId;
        }

        public static UserIdCookie FromValue(string userId) => new UserIdCookie(Guid.Parse(userId));

        public override string ToString() => UserId.ToString();
    }
}