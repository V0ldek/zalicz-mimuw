﻿namespace Zalicz.Mimuw.Cookies
{
    public interface ICookieProvider<out T> where T : new()
    {
        T GetCookie();
    }
}