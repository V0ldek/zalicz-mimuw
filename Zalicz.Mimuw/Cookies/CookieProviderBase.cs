﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Localization;

namespace Zalicz.Mimuw.Cookies
{
    public abstract class CookieProviderBase<T> : ICookieProvider<T> where T : new()
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IStringLocalizer<SharedResources> _sharedLocalizer;

        protected abstract string Key { get; }
        protected abstract CookieOptions Options { get; }

        protected CookieProviderBase(
            IHttpContextAccessor httpContextAccessor,
            IStringLocalizer<SharedResources> sharedLocalizer)
        {
            _httpContextAccessor = httpContextAccessor;
            _sharedLocalizer = sharedLocalizer;
        }

        public virtual T GetCookie()
        {
            var context = _httpContextAccessor.HttpContext;
            AssertConsent(context);

            var cookieValue = context.Request.Cookies[Key];

            return string.IsNullOrEmpty(cookieValue) ? CreateCookie(context) : FromValue(cookieValue);
        }

        protected abstract T FromValue(string value);

        protected void AssertConsent(HttpContext context)
        {
            var consent = context.Features.Get<ITrackingConsentFeature>();

            if (consent != null && !consent.CanTrack)
            {
                throw new CookieConsentException(
                    _sharedLocalizer["This cannot be done without consenting to cookies."].Value);
            }
        }

        protected virtual T CreateCookie(HttpContext context)
        {
            var cookie = new T();

            context.Response.Cookies.Append(Key, cookie.ToString(), Options);

            return cookie;
        }
    }
}