﻿using Microsoft.AspNetCore.Mvc;
using Zalicz.Mimuw.Markdown;

namespace Zalicz.Mimuw.Controllers
{
    public class MarkdownController : Controller
    {
        private readonly IMarkdownParser _markdownParser;

        public MarkdownController(IMarkdownParser markdownParser)
        {
            _markdownParser = markdownParser;
        }

        [HttpPost]
        public IActionResult Parse(string content)
        {
            if (content == null)
            {
                content = "";
            }

            var htmlContent = _markdownParser.ParseToHtml(content);

            return Content(htmlContent.ToString());
        }
    }
}