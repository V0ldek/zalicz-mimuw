﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Services.Courses;
using Zalicz.Mimuw.Models.Course;

namespace Zalicz.Mimuw.Controllers
{
    public class CourseController : Controller
    {
        private readonly ICourseService _courseService;
        private readonly IStringLocalizer<CourseController> _localizer;

        public CourseController(ICourseService courseService, IStringLocalizer<CourseController> localizer)
        {
            _courseService = courseService;
            _localizer = localizer;
        }

        [HttpGet]
        public IActionResult CreateCourse() => View("CreateCourse", new CreateCourseViewModel());

        [HttpGet]
        public async Task<JsonResult> IsShortNameAvailable(string shortName) =>
            await _courseService.IsShortNameAvailableAsync(shortName)
                ? Json(true)
                : Json(string.Format(_localizer["Short name must be unique - course {0} already exists."], shortName));

        [HttpPost]
        public async Task<IActionResult> CreateCourse(CreateCourseViewModel createCourseViewModel)
        {
            await _courseService.CreateCourseAsync(createCourseViewModel.ToModel());

            return RedirectToAction("Search", "Home");
        }
    }
}