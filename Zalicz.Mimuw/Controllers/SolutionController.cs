﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Services.Solutions;
using Services.Tasks;
using Zalicz.Mimuw.Cookies;
using Zalicz.Mimuw.Models.Solution;
using Zalicz.Mimuw.Models.Task;

namespace Zalicz.Mimuw.Controllers
{
    public class SolutionController : Controller
    {
        private readonly IStringLocalizer<SolutionController> _localizer;
        private readonly ISolutionService _solutionService;
        private readonly ITaskService _taskService;
        private readonly ICookieProvider<UserIdCookie> _userIdCookieProvider;

        public SolutionController(
            ICookieProvider<UserIdCookie> userIdCookieProvider,
            ITaskService taskService,
            ISolutionService solutionService,
            IStringLocalizer<SolutionController> localizer)
        {
            _taskService = taskService;
            _solutionService = solutionService;
            _userIdCookieProvider = userIdCookieProvider;
            _localizer = localizer;
        }

        [HttpGet]
        public async Task<IActionResult> CreateSolution(int taskId)
        {
            var taskViewModel = (await _taskService.GetTaskAsync(taskId)).ToViewModel();
            var createSolutionViewModel = new CreateSolutionViewModel(taskViewModel);

            return View("CreateSolution", createSolutionViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> CreateSolution(CreateSolutionViewModel createSolutionViewModel)
        {
            var userId = _userIdCookieProvider.GetCookie().UserId.ToString();
            createSolutionViewModel.Author = userId;
            var createSolutionModel = createSolutionViewModel.ToModel();

            await _solutionService.CreateSolutionAsync(createSolutionModel);

            return RedirectToAction("Search", "Home");
        }

        [HttpGet]
        public async Task<IActionResult> EditSolution(int solutionId)
        {
            var solution = await _solutionService.GetSolutionAsync(solutionId);
            var editSolutionViewModel = solution.ToEditViewModel();

            return View("EditSolution", editSolutionViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> EditSolution(EditSolutionViewModel editSolutionViewModel)
        {
            var userId = _userIdCookieProvider.GetCookie().UserId.ToString();
            editSolutionViewModel.Author = userId;
            var editSolutionModel = editSolutionViewModel.ToModel();

            await _solutionService.EditSolutionAsync(editSolutionModel);

            return RedirectToAction("Search", "Home");
        }

        [HttpPost]
        public JsonResult IsContentUpdateValid(string content, string originalContent) =>
            content.Equals(originalContent) ? Json(_localizer["No changes were made."].Value) : Json(true);
    }
}