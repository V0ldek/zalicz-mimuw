﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Services.NegativeReviews;
using Services.Solutions;
using Zalicz.Mimuw.Cookies;
using Zalicz.Mimuw.Models.NegativeReview;

namespace Zalicz.Mimuw.Controllers
{
    public class NegativeReviewController : Controller
    {
        private readonly IStringLocalizer<NegativeReviewController> _localizer;
        private readonly INegativeReviewService _negativeReviewService;
        private readonly ISolutionService _solutionService;
        private readonly ICookieProvider<UserIdCookie> _userIdCookieProvider;

        public NegativeReviewController(
            ICookieProvider<UserIdCookie> userIdCookieProvider,
            ISolutionService solutionService,
            INegativeReviewService negativeReviewService,
            IStringLocalizer<NegativeReviewController> localizer)
        {
            _userIdCookieProvider = userIdCookieProvider;
            _solutionService = solutionService;
            _negativeReviewService = negativeReviewService;
            _localizer = localizer;
        }

        [HttpGet]
        public async Task<IActionResult> CreateNegativeReview(int solutionId)
        {
            var solution = await _solutionService.GetSolutionAsync(solutionId);
            var createNegativeReviewViewModel = new CreateNegativeReviewViewModel(solution);

            return View("CreateNegativeReview", createNegativeReviewViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> CreateNegativeReview(
            CreateNegativeReviewViewModel createNegativeReviewViewModel)
        {
            var userId = _userIdCookieProvider.GetCookie().UserId.ToString();
            createNegativeReviewViewModel.Author = userId;
            var createNegativeReviewModel = createNegativeReviewViewModel.ToModel();

            await _negativeReviewService.CreateNegativeReviewAsync(createNegativeReviewModel);

            return RedirectToAction("Search", "Home");
        }

        [HttpGet]
        public async Task<IActionResult> EditNegativeReview(int negativeReviewId)
        {
            var negativeReview = await _negativeReviewService.GetNegativeReviewAsync(negativeReviewId);
            var editNegativeReviewViewModel = negativeReview.ToEditViewModel();

            return View("EditNegativeReview", editNegativeReviewViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> EditNegativeReview(EditNegativeReviewViewModel editNegativeReviewViewModel)
        {
            var userId = _userIdCookieProvider.GetCookie().UserId.ToString();
            editNegativeReviewViewModel.Author = userId;
            var editNegativeReviewModel = editNegativeReviewViewModel.ToModel();

            await _negativeReviewService.EditNegativeReviewAsync(editNegativeReviewModel);

            return RedirectToAction("Search", "Home");
        }

        [HttpPost]
        public JsonResult IsContentUpdateValid(string content, string originalContent) =>
            content.Equals(originalContent) ? Json(_localizer["No changes were made."].Value) : Json(true);
    }
}