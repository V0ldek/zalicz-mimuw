﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Services.Courses;
using Services.Tasks;
using Zalicz.Mimuw.Cookies;
using Zalicz.Mimuw.Models.Course;
using Zalicz.Mimuw.Models.Task;

namespace Zalicz.Mimuw.Controllers
{
    public class TaskController : Controller
    {
        private readonly ICourseService _courseService;
        private readonly IStringLocalizer<TaskController> _localizer;
        private readonly ITaskService _taskService;
        private readonly ICookieProvider<UserIdCookie> _userIdCookieProvider;

        public TaskController(
            ICookieProvider<UserIdCookie> userIdCookieProvider,
            ICourseService courseService,
            ITaskService taskService,
            IStringLocalizer<TaskController> localizer)
        {
            _userIdCookieProvider = userIdCookieProvider;
            _courseService = courseService;
            _taskService = taskService;
            _localizer = localizer;
        }

        [HttpGet]
        public async Task<IActionResult> CreateTask()
        {
            var courses = (await _courseService.GetAllCoursesAsync()).Select(c => c.ToViewModel());
            var createTaskViewModel = new CreateTaskViewModel(courses);

            return View("CreateTask", createTaskViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> CreateTask(CreateTaskViewModel createTaskViewModel)
        {
            var userId = _userIdCookieProvider.GetCookie().UserId.ToString();
            createTaskViewModel.Author = userId;

            await _taskService.CreateTaskAsync(createTaskViewModel.ToModel());

            return RedirectToAction("Search", "Home");
        }

        [HttpGet]
        public async Task<IActionResult> EditTask(int taskId)
        {
            var task = await _taskService.GetTaskAsync(taskId);
            var editTaskViewModel = task.ToEditViewModel();

            return View("EditTask", editTaskViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> EditTask(EditTaskViewModel editTaskViewModel)
        {
            var userId = _userIdCookieProvider.GetCookie().UserId.ToString();
            editTaskViewModel.Author = userId;
            var editTaskModel = editTaskViewModel.ToModel();

            await _taskService.EditTaskAsync(editTaskModel);

            return RedirectToAction("Search", "Home");
        }

        [HttpPost]
        public JsonResult IsContentUpdateValid(string content, string originalContent) =>
            content.Equals(originalContent) ? Json(_localizer["No changes were made."].Value) : Json(true);
    }
}