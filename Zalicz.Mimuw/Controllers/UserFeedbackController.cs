﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Services.UserFeedback;
using Zalicz.Mimuw.Cookies;

namespace Zalicz.Mimuw.Controllers
{
    public class UserFeedbackController : Controller
    {
        private readonly IStringLocalizer<UserFeedbackController> _localizer;
        private readonly IUserFeedbackService _userFeedbackService;
        private readonly ICookieProvider<UserIdCookie> _userIdCookieProvider;

        public UserFeedbackController(
            ICookieProvider<UserIdCookie> userIdCookieProvider,
            IUserFeedbackService userFeedbackService,
            IStringLocalizer<UserFeedbackController> localizer)
        {
            _userFeedbackService = userFeedbackService;
            _userIdCookieProvider = userIdCookieProvider;
            _localizer = localizer;
        }

        [HttpPost]
        public async Task<IActionResult> UpvoteSolution(int solutionId)
        {
            string userId;

            try
            {
                userId = _userIdCookieProvider.GetCookie().UserId.ToString();
            }
            catch (CookieConsentException exception)
            {
                return StatusCode((int) HttpStatusCode.Forbidden, exception.Message);
            }

            if (!await _userFeedbackService.CanUserUpvoteSolutionAsync(userId, solutionId))
            {
                return BadRequest(_localizer["This solution was already upvoted by you."].Value);
            }

            await _userFeedbackService.UpvoteSolutionAsync(userId, solutionId);

            return Ok();
        }
    }
}