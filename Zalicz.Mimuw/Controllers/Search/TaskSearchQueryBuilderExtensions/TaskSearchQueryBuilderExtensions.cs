﻿using Services.Tasks.TaskSearchQueries;
using Zalicz.Mimuw.Models.Search;
using Zalicz.Mimuw.Models.Taskset;

namespace Zalicz.Mimuw.Controllers.Search.TaskSearchQueryBuilderExtensions
{
    public static class TaskSearchQueryBuilderExtensions
    {
        public static ITaskSearchQueryBuilder AppendSearchViewModel(
            this ITaskSearchQueryBuilder builder,
            SearchViewModel searchViewModel)
        {
            var (tasksetName, tasksetYear) = TasksetViewModel.DeconstructIdentifier(searchViewModel.TasksetNameAndYear);

            builder.IsInTaskset(searchViewModel.CourseShortName, tasksetName, tasksetYear);

            if (searchViewModel.RequireSolution)
            {
                builder.HasSolution();
            }

            return builder;
        }
    }
}