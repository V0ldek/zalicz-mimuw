﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services.Courses;
using Services.Tasks;
using Zalicz.Mimuw.Controllers.Search.TaskSearchQueryBuilderExtensions;
using Zalicz.Mimuw.Models.Course;
using Zalicz.Mimuw.Models.Search;
using Zalicz.Mimuw.Models.Task;

namespace Zalicz.Mimuw.Controllers.Search
{
    public class SearchController : Controller
    {
        private readonly ICourseService _courseService;
        private readonly ITaskService _taskService;

        public SearchController(ICourseService courseService, ITaskService taskService)
        {
            _courseService = courseService;
            _taskService = taskService;
        }

        [HttpGet]
        public async Task<IActionResult> Search()
        {
            var courses = (await _courseService.GetAllCoursesAsync()).Select(c => c.ToViewModel());
            var searchViewModel = new SearchViewModel(courses);

            return View("Search", searchViewModel);
        }

        [HttpGet]
        public async Task<IActionResult> SearchTasks(SearchViewModel searchViewModel)
        {
            var query = _taskService.GetSearchQueryBuilder()
                .IncludeSolutionsAndReviews()
                .OrderByName()
                .AppendSearchViewModel(searchViewModel)
                .OrderSolutionsByUpvotesDescending()
                .OrderSolutionsNoNegativeReviewsFirst()
                .Build();

            var result = await _taskService.GetTasksAsync(query);

            return ViewComponent("SearchResult", result.Select(t => t.ToViewModel()).ToList());
        }
    }
}