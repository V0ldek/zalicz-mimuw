﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Services.Courses;
using Services.Tasksets;
using Zalicz.Mimuw.Models.Course;
using Zalicz.Mimuw.Models.Taskset;

namespace Zalicz.Mimuw.Controllers
{
    public class TasksetController : Controller
    {
        private readonly ICourseService _courseService;
        private readonly IStringLocalizer<TasksetController> _localizer;
        private readonly ITasksetService _tasksetService;

        public TasksetController(
            ITasksetService tasksetService,
            ICourseService courseService,
            IStringLocalizer<TasksetController> localizer)
        {
            _tasksetService = tasksetService;
            _courseService = courseService;
            _localizer = localizer;
        }

        private static bool IsYearValid(int year) => year > 1900 && year <= DateTime.Now.Year + 1;

        [HttpGet]
        public async Task<JsonResult> IsTasksetNameAndYearValid(
            string courseShortName,
            string name,
            int year)
        {
            if (!IsYearValid(year))
            {
                return Json(_localizer["This year is invalid."]);
            }

            return !await _tasksetService.IsTasksetNameAndYearValidAsync(courseShortName, name, year)
                ? Json(
                    string.Format(
                        _localizer["A taskset with given name and year already exists for the {0} course."],
                        courseShortName))
                : Json(true);
        }

        [HttpGet]
        public async Task<IActionResult> CreateTaskset()
        {
            var courses = (await _courseService.GetAllCoursesAsync()).Select(c => c.ToViewModel());
            var createTasksetViewModel = new CreateTasksetViewModel(courses);

            return View("CreateTaskset", createTasksetViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> CreateTaskset(CreateTasksetViewModel createTasksetViewModel)
        {
            await _tasksetService.CreateTasksetAsync(
                createTasksetViewModel.CourseShortName,
                createTasksetViewModel.ToModel());

            return RedirectToAction("Search", "Home");
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<SelectListItem>>> CourseTasksetsSelectListItems(
            string courseShortName) => new ActionResult<IEnumerable<SelectListItem>>(
            (await _courseService.GetAllTasksetsForCourseAsync(courseShortName)).Select(
                t => t.ToViewModel().ToSelectListItem()));
    }
}