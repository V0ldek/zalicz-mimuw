﻿using System.Net;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zalicz.Mimuw.Cookies;
using Zalicz.Mimuw.Models;

namespace Zalicz.Mimuw.Controllers
{
    public class ErrorController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ErrorController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            var context = _httpContextAccessor.HttpContext;
            var exceptionFeature = context.Features.Get<IExceptionHandlerPathFeature>();

            if (exceptionFeature?.Error is CookieConsentException)
            {
                context.Response.StatusCode = (int) HttpStatusCode.Forbidden;
            }

            var errorViewModel = new ErrorViewModel(
                (HttpStatusCode) context.Response.StatusCode,
                exceptionFeature?.Error.Message);

            return View(errorViewModel);
        }
    }
}