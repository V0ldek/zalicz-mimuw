﻿using Microsoft.AspNetCore.Mvc;

namespace Zalicz.Mimuw.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Search() => RedirectToAction("Search", "Search");
    }
}