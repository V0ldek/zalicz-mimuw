﻿
# <center>Zalicz.Mimuw</center>
## Vision Doc v1.0.0
###### by Mateusz Gienieczko, last edited on: 18.11.2018

### Description

**Zalicz.Mimuw** is a university test/exam task database and search engine. 

### Motivation

Whenever there's a test or an exam coming, a post on the year's Facebook group appears saying

> Does anyone have the previous year's test/exam and would share it?

Often it's followed by a bunch of comments with JPGs of old tests. There are also instances of students creating separate Google Spreadsheets with lists of tasks and comments trying to explain the solutions. Overall this effort is disorganized, and so **Zalicz.Mimuw**'s main objective is to centralize this efforts and make exam preparation more efficient, helping the students to actually Pass.Mimuw.

There's one exception - there's the [Wakacje z Matematyką Dyskretną](http://wakacjezmd.wikidot.com/) site, which is a centralized hub for tasks and solutions for that one course - Discrete Mathematics. **Zalicz.Mimuw** is building on that idea to provide a solution for many different courses and in a more specialized way (after all Wakacje z Matematyką Dyskretną is a wikidot webpage, with all the limitations that implies).

### Core functionalities

The following functionalities are considered essential for the app to present any value to the users. 

1. User should be able to search the database for tasks using search criteria like: the course, whether it was a test or the final exam task, wheter it has a solution description, year, etc.

   - Imagine a student trying to study for his upcoming first test in Algorithms and Data Structures. He/she should be able to select filters `Course = Algorithms and Data Structures`, `Type = Test`, `Has solution = True` to find all tasks from past ADS tests that have a solution in the database.
   - That implies that the tasks have to be tagged accordingly.
   
2. The app should store task descriptions and possibly its solutions in $\LaTeX$ (or Markdown with inline $\LaTeX$ support) and display them as compiled, human readable documents.

3. The developer/maintainer should be able to add new tasks and solutions to the system via a specialized entry point.

### Further functionalities

The following functionalities are important (in no way are they "additional", which is too often synonymical with "negligible" functionalities), but failure to deliver one of them will not render the application useless.

1. User should be able to submit their own task or solution to the system and edit existing solutions.

   - Wiki-style contribution, edit history has to be mantained.

2. User should be able to upvote existing solutions as helpful and correct or point out mistakes in them via edits.

3. User should be able to look for entire task-set of a test or an exam.

   - Going back to our example student studying for ADS, he/she should be able to search for `Taskset = Exam 2016` to find all tasks that were given during the 2016 exam.

### Data sources

The system will be initially populated by the developer with a set of tasks from various sources, Google Spreadsheets, Facebook posts, the legendary Wakacje z Matematyką Dyskretną webpage and countless other means MIMUW students were using to share tasks and solutions that were mentioned in the introduction. Later on the application depends on the dev or the users submitting their tasks and solutions as they appear.
