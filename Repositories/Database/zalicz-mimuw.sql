﻿-- Zalicz.Mimuw database schema.
-- Requires PostgreSQL 10.6 or later.

-- TABLES

CREATE TABLE course
(
  id         INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  short_name VARCHAR(8) UNIQUE  NOT NULL,
  long_name  VARCHAR(64) UNIQUE NOT NULL
);

CREATE TABLE taskset
(
  id        INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  course_id INTEGER REFERENCES course (id) ON DELETE CASCADE               NOT NULL,
  year      INTEGER CHECK (year BETWEEN 1900 AND EXTRACT(YEAR FROM NOW())) NOT NULL,
  name      VARCHAR(32)                                                    NOT NULL,

  CONSTRAINT no_identical_tasksets UNIQUE (course_id, year, name)
);

CREATE TABLE task
(
  id         INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  taskset_id INTEGER REFERENCES taskset (id) ON DELETE CASCADE NOT NULL,
  name       VARCHAR(32)                                       NOT NULL
);

CREATE TABLE solution
(
  id      INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  task_id INTEGER REFERENCES task (id) ON DELETE CASCADE NOT NULL
);

CREATE TABLE negative_review
(
  id                 INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  solution_id        INTEGER REFERENCES solution (id) ON DELETE CASCADE NOT NULL,
  rejected_timestamp TIMESTAMP                                          NULL -- NULL means not rejected.
);

CREATE TABLE content_version
(
  version           INTEGER                 NOT NULL,
  content           TEXT                    NOT NULL,
  author            VARCHAR(36)             NOT NULL,
  created_timestamp TIMESTAMP DEFAULT NOW() NOT NULL,
  is_head           BOOLEAN                 NOT NULL
);

CREATE TABLE task_content_version
(
  id      INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  task_id INTEGER REFERENCES task (id) ON DELETE CASCADE NOT NULL
)
  INHERITS (content_version);

CREATE TABLE solution_content_version
(
  id          INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  solution_id INTEGER REFERENCES solution (id) ON DELETE CASCADE NOT NULL
)
  INHERITS (content_version);

CREATE TABLE negative_review_content_version
(
  id                 INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  negative_review_id INTEGER REFERENCES negative_review (id) ON DELETE CASCADE NOT NULL
)
  INHERITS (content_version);

CREATE TABLE upvote
(
  solution_id INTEGER REFERENCES solution (id) ON DELETE CASCADE NOT NULL,
  author      VARCHAR(36)                                        NOT NULL,

  PRIMARY KEY (solution_id, author)
);

-- INDICES

CREATE UNIQUE INDEX task_content_version_heads_idx
  ON
    task_content_version (task_id)
  WHERE is_head IS TRUE;

CREATE UNIQUE INDEX solution_content_version_heads_idx
  ON
    solution_content_version (solution_id)
  WHERE is_head IS TRUE;

CREATE UNIQUE INDEX negative_review_content_version_heads_idx
  ON
    negative_review_content_version (negative_review_id)
  WHERE is_head IS TRUE;

-- VIEWS

CREATE OR REPLACE VIEW task_view AS
  SELECT t.id,
         t.name,
         t.taskset_id,
         c.content,
         c.id                content_version_id,
         c.created_timestamp edited_timestamp,
         c.version
  FROM task t
         JOIN task_content_version c ON c.task_id = t.id
  WHERE c.is_head IS TRUE;

CREATE OR REPLACE VIEW solution_view AS
  SELECT s.id,
         s.task_id,
         c.content,
         c.id                content_version_id,
         c.created_timestamp edited_timestamp,
         c.version,
         COUNT(u.author)     upvote_count
  FROM solution s
         JOIN solution_content_version c ON c.solution_id = s.id
         LEFT JOIN upvote u ON s.id = u.solution_id
  WHERE c.is_head IS TRUE
  GROUP BY s.id, s.task_id, c.content, c.id, c.created_timestamp, c.version;

CREATE OR REPLACE VIEW negative_review_view AS
  SELECT n.id,
         n.solution_id,
         n.rejected_timestamp,
         c.content,
         c.id                content_version_id,
         c.created_timestamp edited_timestamp,
         c.version
  FROM negative_review n
         JOIN negative_review_content_version c ON c.negative_review_id = n.id
  WHERE c.is_head IS TRUE;

-- TRIGGERS

CREATE OR REPLACE FUNCTION assert_task_has_head_content() RETURNS TRIGGER AS
$$
BEGIN
  IF (NOT EXISTS(SELECT id FROM task_content_version WHERE is_head IS TRUE AND task_id = NEW.id)) THEN
    RAISE EXCEPTION 'Task with id % has no head content.', tg_argv[0];
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION assert_solution_has_head_content() RETURNS TRIGGER AS
$$
BEGIN
  IF (NOT EXISTS(SELECT id FROM solution_content_version WHERE is_head IS TRUE AND solution_id = NEW.id)) THEN
    RAISE EXCEPTION 'Solution with id % has no head content.', tg_argv[0];
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION assert_negative_review_has_head_content() RETURNS TRIGGER AS
$$
BEGIN
  IF (NOT EXISTS(
      SELECT id FROM negative_review_content_version WHERE is_head IS TRUE AND negative_review_id = NEW.id)) THEN
    RAISE EXCEPTION 'negative_review with id % has no head content.', tg_argv[0];
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER task_inserted_tg
  AFTER INSERT
  ON task
  DEFERRABLE INITIALLY DEFERRED
  FOR EACH ROW
EXECUTE PROCEDURE assert_task_has_head_content();

CREATE CONSTRAINT TRIGGER solution_inserted_tg
  AFTER INSERT
  ON solution
  DEFERRABLE INITIALLY DEFERRED
  FOR EACH ROW
EXECUTE PROCEDURE assert_solution_has_head_content();

CREATE CONSTRAINT TRIGGER negative_review_inserted_tg
  AFTER INSERT
  ON negative_review
  DEFERRABLE INITIALLY DEFERRED
  FOR EACH ROW
EXECUTE PROCEDURE assert_negative_review_has_head_content();

-- FUNCTIONS

CREATE OR REPLACE FUNCTION create_task(
      referenced_taskset_id       INTEGER,
      new_name                    VARCHAR(32),
      new_content                 TEXT,
      new_author                  VARCHAR(36),
  OUT new_task_id                 INTEGER,
  OUT new_task_content_version_id INTEGER)
AS $$
BEGIN
  INSERT INTO task (taskset_id, name) VALUES (referenced_taskset_id, new_name) RETURNING id
    INTO new_task_id;

  INSERT INTO task_content_version (task_id, content, author, version, is_head)
  VALUES (new_task_id, new_content, new_author, 1, TRUE)
   RETURNING id
     INTO new_task_content_version_id;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION create_solution(
      referenced_task_id              INTEGER,
      new_content                     TEXT,
      new_author                      VARCHAR(36),
  OUT new_solution_id                 INTEGER,
  OUT new_solution_content_version_id INTEGER)
AS $$
BEGIN
  INSERT INTO solution (task_id) VALUES (referenced_task_id) RETURNING id
    INTO new_solution_id;

  INSERT INTO solution_content_version (solution_id, content, author, version, is_head)
  VALUES (new_solution_id, new_content, new_author, 1, TRUE)
   RETURNING id
     INTO new_solution_content_version_id;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION create_negative_review(
      referenced_solution_id                 INTEGER,
      new_content                            TEXT,
      new_author                             VARCHAR(36),
  OUT new_negative_review_id                 INTEGER,
  OUT new_negative_review_content_version_id INTEGER)
AS $$
BEGIN
  INSERT INTO negative_review (solution_id)
  VALUES (referenced_solution_id) RETURNING id
    INTO new_negative_review_id;

  INSERT INTO negative_review_content_version (negative_review_id, content, author, version, is_head)
  VALUES (new_negative_review_id, new_content, new_author, 1, TRUE)
   RETURNING id
     INTO new_negative_review_content_version_id;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_task_content_version(
      original_id                 INTEGER,
      new_content                 TEXT,
      new_author                  VARCHAR(36),
  OUT new_task_content_version_id INTEGER)
AS $$
DECLARE
  original_task_id INTEGER;
  original_version INTEGER;
  original_is_head BOOLEAN;
BEGIN
  SELECT task_id, version, is_head
  FROM task_content_version c
  WHERE c.id = original_id INTO original_task_id, original_version, original_is_head;

  IF (NOT original_is_head)
  THEN
    RAISE EXCEPTION 'Cannot update non-head task content version with id %.', original_id;
  END IF;

  UPDATE task_content_version SET is_head = FALSE WHERE id = original_id;

  INSERT INTO task_content_version (task_id, version, content, author, is_head)
  VALUES (original_task_id, original_version + 1, new_content, new_author, TRUE)
   RETURNING id
     INTO new_task_content_version_id;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_solution_content_version(
      original_id                     INTEGER,
      new_content                     TEXT,
      new_author                      VARCHAR(36),
  OUT new_solution_content_version_id INTEGER) AS $$
DECLARE
  original_solution_id INTEGER;
  original_version     INTEGER;
  original_is_head     BOOLEAN;
BEGIN
  SELECT solution_id, version, is_head
  FROM solution_content_version c
  WHERE c.id = original_id INTO original_solution_id, original_version, original_is_head;

  IF (NOT original_is_head)
  THEN
    RAISE EXCEPTION 'Cannot update non-head solution content version with id %.', original_id;
  END IF;

  UPDATE solution_content_version SET is_head = FALSE WHERE id = original_id;

  INSERT INTO solution_content_version (solution_id, version, content, author, is_head)
  VALUES (original_solution_id, original_version + 1, new_content, new_author, TRUE)
   RETURNING id
     INTO new_solution_content_version_id;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_negative_review_content_version(
      original_id                            INTEGER,
      new_content                            TEXT,
      new_author                             VARCHAR(36),
  OUT new_negative_review_content_version_id INTEGER)
AS $$
DECLARE
  original_negative_review_id INTEGER;
  original_version            INTEGER;
  original_is_head            BOOLEAN;
BEGIN
  SELECT negative_review_id, version, is_head
  FROM negative_review_content_version c
  WHERE c.id = original_id INTO original_negative_review_id, original_version, original_is_head;

  IF (NOT original_is_head)
  THEN
    RAISE EXCEPTION 'Cannot update non-head negative review content version with id %.', original_id;
  END IF;

  UPDATE negative_review_content_version SET is_head = FALSE WHERE id = original_id;

  INSERT INTO negative_review_content_version (negative_review_id, version, content, author, is_head)
  VALUES (original_negative_review_id, original_version + 1, new_content, new_author, TRUE)
   RETURNING id
     INTO new_negative_review_content_version_id;
END;
$$
LANGUAGE plpgsql;

-- ROLES AND PRIVILEGES

DROP ROLE IF EXISTS zalicz_mimuw;

CREATE ROLE zalicz_mimuw
WITH
  LOGIN
  PASSWORD 'postgres'; -- Default password, should be changed.

GRANT EXECUTE
ON ALL FUNCTIONS IN SCHEMA public
TO zalicz_mimuw;

GRANT SELECT, INSERT, UPDATE, DELETE
ON task, solution, negative_review,
task_content_version, solution_content_version, negative_review_content_version,
course, taskset, upvote
TO zalicz_mimuw;

GRANT SELECT
ON task_view, solution_view, negative_review_view
TO zalicz_mimuw;

REVOKE INSERT (created_timestamp), UPDATE (created_timestamp), UPDATE (task_id)
ON task_content_version
FROM PUBLIC;

REVOKE INSERT (created_timestamp), UPDATE (created_timestamp), UPDATE (solution_id)
ON solution_content_version
FROM PUBLIC;

REVOKE INSERT (created_timestamp), UPDATE (created_timestamp), UPDATE (negative_review_id)
ON negative_review_content_version
FROM PUBLIC;
