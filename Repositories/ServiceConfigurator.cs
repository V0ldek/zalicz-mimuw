﻿using System;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Repositories.Contexts;
using Repositories.Repositories;
using Utilities.Assertions;

namespace Repositories
{
    public static class ServiceConfigurator
    {
        public static void ConfigureDbContext(IServiceCollection services)
        {
            string PostgreSqlUrlToConnectionString(string url)
            {
                var regex = new Regex(@"^postgres://(.+):(.+)@(.+):(.+)/(.+)$");
                var match = regex.Match(url);

                Assert.IsTrue(match.Success, $"Could not match PostgreSQL URL {url} to connection string regex.");

                var username = match.Groups[1];
                var password = match.Groups[2];
                var host = match.Groups[3];
                var port = match.Groups[4];
                var database = match.Groups[5];

                return $"Host={host};Port={port};Database={database};Username={username};Password={password}";
            }

            var connectionUrl = Environment.GetEnvironmentVariable("DATABASE_URL");
            Assert.IsNotNull(connectionUrl, nameof(connectionUrl));
            var connectionString = PostgreSqlUrlToConnectionString(connectionUrl);

            services.AddDbContext<ZaliczMimuwContext>(
                options =>
                    options.UseNpgsql(connectionString));

            services.AddScoped<IZaliczMimuwRepository>(provider => provider.GetService<ZaliczMimuwContext>());
        }
    }
}