﻿using System.Collections.Generic;

namespace Repositories.Entities
{
    public class Solution
    {
        public int Id { get; set; }

        public int TaskId { get; set; }
        public virtual Task Task { get; set; }

        public virtual ICollection<SolutionContentVersion> SolutionContentVersions { get; set; } =
            new HashSet<SolutionContentVersion>();

        public virtual ICollection<NegativeReview> NegativeReviews { get; set; } = new HashSet<NegativeReview>();

        public virtual ICollection<Upvote> Upvotes { get; set; } = new HashSet<Upvote>();
    }
}