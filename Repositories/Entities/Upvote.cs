﻿namespace Repositories.Entities
{
    public class Upvote
    {
        public string Author { get; set; }

        public int SolutionId { get; set; }
        public virtual Solution Solution { get; set; }
    }
}