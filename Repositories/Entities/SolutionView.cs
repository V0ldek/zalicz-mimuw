﻿using System;
using System.Collections.Generic;

namespace Repositories.Entities
{
    public class SolutionView
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime EditedTimestamp { get; set; }
        public int Version { get; set; }
        public int UpvoteCount { get; set; }

        public int TaskId { get; set; }
        public TaskView TaskView { get; set; }

        public int ContentVersionId { get; set; }
        public SolutionContentVersion ContentVersion { get; set; }

        public IEnumerable<NegativeReviewView> NegativeReviewViews { get; set; } = new HashSet<NegativeReviewView>();

        // Navigation to the actual row in the solution table.
        public Solution Solution { get; set; }
    }
}