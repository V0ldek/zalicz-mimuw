﻿using System;

namespace Repositories.Entities
{
    public class NegativeReviewView
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime EditedTimestamp { get; set; }
        public DateTime? RejectedTimestamp { get; set; }
        public int Version { get; set; }

        public int SolutionId { get; set; }
        public SolutionView SolutionView { get; set; }

        public int ContentVersionId { get; set; }
        public NegativeReviewContentVersion ContentVersion { get; set; }

        // Navigation to the actual row in the negative_review table.
        public NegativeReview NegativeReview { get; set; }
    }
}