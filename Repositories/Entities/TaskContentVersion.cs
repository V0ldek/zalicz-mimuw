﻿using System;

namespace Repositories.Entities
{
    public class TaskContentVersion
    {
        public int Id { get; set; }
        public int Version { get; set; }
        public string Content { get; set; }
        public string Author { get; set; }
        public DateTime? CreatedTimestamp { get; set; }
        public bool IsHead { get; set; }

        public int TaskId { get; set; }
        public virtual Task Task { get; set; }
    }
}