﻿using System;
using System.Collections.Generic;

namespace Repositories.Entities
{
    public class NegativeReview
    {
        public int Id { get; set; }
        public DateTime? RejectedTimestamp { get; set; }

        public int SolutionId { get; set; }
        public virtual Solution Solution { get; set; }

        public virtual ICollection<NegativeReviewContentVersion> NegativeReviewContentVersions { get; set; } =
            new HashSet<NegativeReviewContentVersion>();
    }
}