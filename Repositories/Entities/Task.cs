﻿using System.Collections.Generic;

namespace Repositories.Entities
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int TasksetId { get; set; }
        public virtual Taskset Taskset { get; set; }

        public virtual ICollection<TaskContentVersion> TaskContentVersions { get; set; } =
            new HashSet<TaskContentVersion>();

        public virtual ICollection<Solution> Solutions { get; set; } = new HashSet<Solution>();
    }
}