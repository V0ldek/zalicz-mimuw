﻿using System;

namespace Repositories.Entities
{
    public class SolutionContentVersion
    {
        public int Id { get; set; }
        public int Version { get; set; }
        public string Content { get; set; }
        public string Author { get; set; }
        public DateTime? CreatedTimestamp { get; set; }
        public bool IsHead { get; set; }

        public int SolutionId { get; set; }
        public virtual Solution Solution { get; set; }
    }
}