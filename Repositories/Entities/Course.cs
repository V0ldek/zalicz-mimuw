﻿using System.Collections.Generic;

namespace Repositories.Entities
{
    public class Course
    {
        public int Id { get; set; }
        public string ShortName { get; set; }
        public string LongName { get; set; }

        public virtual ICollection<Taskset> Tasksets { get; set; } = new HashSet<Taskset>();
    }
}