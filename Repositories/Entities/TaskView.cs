﻿using System;
using System.Collections.Generic;

namespace Repositories.Entities
{
    public class TaskView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public DateTime EditedTimestamp { get; set; }
        public int Version { get; set; }

        public int TasksetId { get; set; }
        public Taskset Taskset { get; set; }

        public int ContentVersionId { get; set; }
        public TaskContentVersion ContentVersion { get; set; }

        public IEnumerable<SolutionView> SolutionViews { get; set; } = new HashSet<SolutionView>();

        // Navigation to the actual row in the task table.
        public Task Task { get; set; }
    }
}