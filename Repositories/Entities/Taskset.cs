﻿using System.Collections.Generic;

namespace Repositories.Entities
{
    public class Taskset
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public string Name { get; set; }

        public int CourseId { get; set; }
        public virtual Course Course { get; set; }

        public virtual ICollection<Task> Tasks { get; set; } = new HashSet<Task>();
    }
}