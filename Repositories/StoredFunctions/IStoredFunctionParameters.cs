﻿using Npgsql;

namespace Repositories.StoredFunctions
{
    internal interface IStoredFunctionParameters
    {
        string Placeholders { get; }

        void FillNpgsqlParameters(NpgsqlParameterCollection parameters);
    }
}