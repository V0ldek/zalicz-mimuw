﻿using Npgsql;

namespace Repositories.StoredFunctions
{
    internal sealed class StoredFunctionParameters<T> : IStoredFunctionParameters
    {
        private readonly T _parameter;

        public StoredFunctionParameters(T parameter)
        {
            _parameter = parameter;
            Placeholders = "@p0";
        }

        public string Placeholders { get; }

        public void FillNpgsqlParameters(NpgsqlParameterCollection parameters) =>
            parameters.AddWithValue("p0", _parameter);
    }

    internal sealed class StoredFunctionParameters<T1, T2> : IStoredFunctionParameters
    {
        private readonly T1 _parameter1;
        private readonly T2 _parameter2;

        public StoredFunctionParameters(T1 parameter1, T2 parameter2)
        {
            _parameter1 = parameter1;
            _parameter2 = parameter2;
            Placeholders = "@p0, @p1";
        }

        public string Placeholders { get; }

        public void FillNpgsqlParameters(NpgsqlParameterCollection parameters)
        {
            parameters.AddWithValue("p0", _parameter1);
            parameters.AddWithValue("p1", _parameter2);
        }
    }

    internal sealed class StoredFunctionParameters<T1, T2, T3> : IStoredFunctionParameters
    {
        private readonly T1 _parameter1;
        private readonly T2 _parameter2;
        private readonly T3 _parameter3;

        public StoredFunctionParameters(T1 parameter1, T2 parameter2, T3 parameter3)
        {
            _parameter1 = parameter1;
            _parameter2 = parameter2;
            _parameter3 = parameter3;
            Placeholders = "@p0, @p1, @p2";
        }

        public string Placeholders { get; }

        public void FillNpgsqlParameters(NpgsqlParameterCollection parameters)
        {
            parameters.AddWithValue("p0", _parameter1);
            parameters.AddWithValue("p1", _parameter2);
            parameters.AddWithValue("p2", _parameter3);
        }
    }

    internal sealed class StoredFunctionParameters<T1, T2, T3, T4> : IStoredFunctionParameters
    {
        private readonly T1 _parameter1;
        private readonly T2 _parameter2;
        private readonly T3 _parameter3;
        private readonly T4 _parameter4;

        public StoredFunctionParameters(T1 parameter1, T2 parameter2, T3 parameter3, T4 parameter4)
        {
            _parameter1 = parameter1;
            _parameter2 = parameter2;
            _parameter3 = parameter3;
            _parameter4 = parameter4;
            Placeholders = "@p0, @p1, @p2, @p3";
        }

        public string Placeholders { get; }

        public void FillNpgsqlParameters(NpgsqlParameterCollection parameters)
        {
            parameters.AddWithValue("p0", _parameter1);
            parameters.AddWithValue("p1", _parameter2);
            parameters.AddWithValue("p2", _parameter3);
            parameters.AddWithValue("p3", _parameter4);
        }
    }
}