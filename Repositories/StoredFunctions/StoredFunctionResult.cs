﻿using Utilities.Assertions;

namespace Repositories.StoredFunctions
{
    internal class StoredFunctionResult<T> : IStoredFunctionResult
    {
        private T _resultValue;

        public void LoadResult(object[] result)
        {
            Assert.IsTrue(result.Length == 1, $"LoadResult input is of length {result.Length}, expected 1.");

            _resultValue = (T) result[0];
        }

        public T ToTuple() => _resultValue;
    }

    internal class StoredFunctionResult<T1, T2> : IStoredFunctionResult
    {
        private T1 _resultValue1;
        private T2 _resultValue2;

        public void LoadResult(object[] result)
        {
            Assert.IsTrue(result.Length == 2, $"LoadResult input is of length {result.Length}, expected 2.");

            _resultValue1 = (T1) result[0];
            _resultValue2 = (T2) result[1];
        }

        public (T1, T2) ToTuple() => (_resultValue1, _resultValue2);
    }
}