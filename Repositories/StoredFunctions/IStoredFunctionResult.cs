﻿namespace Repositories.StoredFunctions
{
    internal interface IStoredFunctionResult
    {
        void LoadResult(object[] result);
    }
}