﻿using System.Data;
using System.Threading.Tasks;
using Npgsql;
using Utilities.Assertions;

namespace Repositories.StoredFunctions
{
    internal sealed class StoredFunction<TParameters, TResult>
        where TParameters : IStoredFunctionParameters
        where TResult : IStoredFunctionResult, new()
    {
        private readonly string _functionName;

        public StoredFunction(string functionName)
        {
            _functionName = functionName;
        }

        public async Task<TResult> ExecuteAsync(TParameters parameters, NpgsqlConnection databaseConnection)
        {
            var command = $"SELECT {_functionName}({parameters.Placeholders})";
            var result = new TResult();

            using (var cmd = new NpgsqlCommand(command, databaseConnection))
            {
                parameters.FillNpgsqlParameters(cmd.Parameters);

                if (databaseConnection.State != ConnectionState.Open)
                {
                    await databaseConnection.OpenAsync();
                }

                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    Assert.IsTrue(
                        await reader.ReadAsync(),
                        $"StoredFunction {_functionName} execution yielded no results.");

                    var resultRow = reader[0] as object[] ?? new[] {reader[0]};
                    result.LoadResult(resultRow);

                    Assert.IsFalse(
                        await reader.ReadAsync(),
                        $"StoredFunction {_functionName} execution yielded more than one result row.");
                }
            }

            return result;
        }
    }
}