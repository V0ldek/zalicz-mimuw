﻿using Microsoft.EntityFrameworkCore;
using Repositories.Entities;

namespace Repositories.Contexts
{
    public partial class ZaliczMimuwContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<Course>(
                entity =>
                {
                    entity.ToTable("course");

                    entity.HasKey(e => e.Id);

                    entity.HasIndex(e => e.LongName)
                        .HasName("course_long_name_key")
                        .IsUnique();
                    entity.HasIndex(e => e.ShortName)
                        .HasName("course_short_name_key")
                        .IsUnique();

                    entity.Property(e => e.Id)
                        .HasColumnName("id")
                        .UseNpgsqlIdentityAlwaysColumn();
                    entity.Property(e => e.LongName)
                        .IsRequired()
                        .HasColumnName("long_name")
                        .HasMaxLength(64);
                    entity.Property(e => e.ShortName)
                        .IsRequired()
                        .HasColumnName("short_name")
                        .HasMaxLength(8);
                });

            modelBuilder.Entity<NegativeReview>(
                entity =>
                {
                    entity.ToTable("negative_review");

                    entity.HasKey(e => e.Id);

                    entity.Property(e => e.Id)
                        .HasColumnName("id")
                        .UseNpgsqlIdentityAlwaysColumn();
                    entity.Property(e => e.RejectedTimestamp).HasColumnName("rejected_timestamp");
                    entity.Property(e => e.SolutionId).HasColumnName("solution_id");

                    entity.HasOne(d => d.Solution)
                        .WithMany(p => p.NegativeReviews)
                        .HasForeignKey(d => d.SolutionId)
                        .HasConstraintName("negative_review_solution_id_fkey");
                });

            modelBuilder.Entity<NegativeReviewContentVersion>(
                entity =>
                {
                    entity.ToTable("negative_review_content_version");

                    entity.HasKey(e => e.Id);

                    entity.HasIndex(e => e.NegativeReviewId)
                        .HasName("negative_review_content_version_heads_idx")
                        .IsUnique()
                        .HasFilter("(is_head IS TRUE)");

                    entity.Property(e => e.Id)
                        .HasColumnName("id")
                        .UseNpgsqlIdentityAlwaysColumn();
                    entity.Property(e => e.Author)
                        .IsRequired()
                        .HasColumnName("author")
                        .HasMaxLength(36);
                    entity.Property(e => e.Content)
                        .IsRequired()
                        .HasColumnName("content");
                    entity.Property(e => e.CreatedTimestamp)
                        .HasColumnName("created_timestamp")
                        .HasDefaultValueSql("now()");
                    entity.Property(e => e.IsHead).HasColumnName("is_head");
                    entity.Property(e => e.NegativeReviewId).HasColumnName("negative_review_id");
                    entity.Property(e => e.Version).HasColumnName("version");

                    entity.HasOne(d => d.NegativeReview)
                        .WithMany(p => p.NegativeReviewContentVersions)
                        .HasForeignKey(d => d.NegativeReviewId)
                        .HasConstraintName("negative_review_content_version_negative_review_id_fkey");
                });

            modelBuilder.Entity<NegativeReviewView>(
                entity =>
                {
                    entity.ToTable("negative_review_view");

                    entity.HasKey(e => e.Id);

                    entity.Property(e => e.Id).HasColumnName("id");
                    entity.Property(e => e.SolutionId).HasColumnName("solution_id");
                    entity.Property(e => e.Content).HasColumnName("content");
                    entity.Property(e => e.ContentVersionId).HasColumnName("content_version_id");
                    entity.Property(e => e.EditedTimestamp).HasColumnName("edited_timestamp");
                    entity.Property(e => e.RejectedTimestamp).HasColumnName("rejected_timestamp");
                    entity.Property(e => e.Version).HasColumnName("version");

                    entity.HasOne(d => d.NegativeReview)
                        .WithOne()
                        .HasForeignKey<NegativeReviewView>(d => d.Id);
                    entity.HasOne(d => d.SolutionView)
                        .WithMany(d => d.NegativeReviewViews)
                        .HasForeignKey(d => d.SolutionId);
                    entity.HasOne(d => d.ContentVersion)
                        .WithOne()
                        .HasForeignKey<NegativeReviewView>(d => d.ContentVersionId);
                });

            modelBuilder.Entity<Solution>(
                entity =>
                {
                    entity.ToTable("solution");

                    entity.HasKey(e => e.Id);

                    entity.Property(e => e.Id)
                        .HasColumnName("id")
                        .UseNpgsqlIdentityAlwaysColumn();
                    entity.Property(e => e.TaskId).HasColumnName("task_id");

                    entity.HasOne(d => d.Task)
                        .WithMany(p => p.Solutions)
                        .HasForeignKey(d => d.TaskId)
                        .HasConstraintName("solution_task_id_fkey");
                });

            modelBuilder.Entity<SolutionContentVersion>(
                entity =>
                {
                    entity.ToTable("solution_content_version");

                    entity.HasKey(e => e.Id);

                    entity.HasIndex(e => e.SolutionId)
                        .HasName("solution_content_version_heads_idx")
                        .IsUnique()
                        .HasFilter("(is_head IS TRUE)");

                    entity.Property(e => e.Id)
                        .HasColumnName("id")
                        .UseNpgsqlIdentityAlwaysColumn();
                    entity.Property(e => e.Author)
                        .IsRequired()
                        .HasColumnName("author")
                        .HasMaxLength(36);
                    entity.Property(e => e.Content)
                        .IsRequired()
                        .HasColumnName("content");
                    entity.Property(e => e.CreatedTimestamp)
                        .HasColumnName("created_timestamp")
                        .HasDefaultValueSql("now()");
                    entity.Property(e => e.IsHead).HasColumnName("is_head");
                    entity.Property(e => e.SolutionId).HasColumnName("solution_id");
                    entity.Property(e => e.Version).HasColumnName("version");

                    entity.HasOne(d => d.Solution)
                        .WithMany(p => p.SolutionContentVersions)
                        .HasForeignKey(d => d.SolutionId)
                        .HasConstraintName("solution_content_version_solution_id_fkey");
                });

            modelBuilder.Entity<SolutionView>(
                entity =>
                {
                    entity.ToTable("solution_view");

                    entity.HasKey(e => e.Id);

                    entity.Property(e => e.Id).HasColumnName("id");
                    entity.Property(e => e.TaskId).HasColumnName("task_id");
                    entity.Property(e => e.Content).HasColumnName("content");
                    entity.Property(e => e.ContentVersionId).HasColumnName("content_version_id");
                    entity.Property(e => e.EditedTimestamp).HasColumnName("edited_timestamp");
                    entity.Property(e => e.Version).HasColumnName("version");
                    entity.Property(e => e.UpvoteCount).HasColumnName("upvote_count");

                    entity.HasOne(d => d.Solution)
                        .WithOne()
                        .HasForeignKey<SolutionView>(d => d.Id);
                    entity.HasOne(d => d.TaskView)
                        .WithMany(d => d.SolutionViews)
                        .HasForeignKey(d => d.TaskId);
                    entity.HasOne(d => d.ContentVersion)
                        .WithOne()
                        .HasForeignKey<SolutionView>(d => d.ContentVersionId);
                });

            modelBuilder.Entity<Task>(
                entity =>
                {
                    entity.ToTable("task");

                    entity.HasKey(e => e.Id);

                    entity.Property(e => e.Id)
                        .HasColumnName("id")
                        .UseNpgsqlIdentityAlwaysColumn();
                    entity.Property(e => e.TasksetId).HasColumnName("taskset_id");
                    entity.Property(e => e.Name).HasColumnName("name")
                        .HasMaxLength(36);

                    entity.HasOne(d => d.Taskset)
                        .WithMany(p => p.Tasks)
                        .HasForeignKey(d => d.TasksetId)
                        .HasConstraintName("task_taskset_id_fkey");
                });

            modelBuilder.Entity<TaskContentVersion>(
                entity =>
                {
                    entity.ToTable("task_content_version");

                    entity.HasKey(e => e.Id);

                    entity.HasIndex(e => e.TaskId)
                        .HasName("task_content_version_heads_idx")
                        .IsUnique()
                        .HasFilter("(is_head IS TRUE)");

                    entity.Property(e => e.Id)
                        .HasColumnName("id")
                        .UseNpgsqlIdentityAlwaysColumn();
                    entity.Property(e => e.Author)
                        .IsRequired()
                        .HasColumnName("author")
                        .HasMaxLength(36);
                    entity.Property(e => e.Content)
                        .IsRequired()
                        .HasColumnName("content");
                    entity.Property(e => e.CreatedTimestamp)
                        .HasColumnName("created_timestamp")
                        .HasDefaultValueSql("now()");
                    entity.Property(e => e.IsHead).HasColumnName("is_head");
                    entity.Property(e => e.TaskId).HasColumnName("task_id");
                    entity.Property(e => e.Version).HasColumnName("version");

                    entity.HasOne(d => d.Task)
                        .WithMany(p => p.TaskContentVersions)
                        .HasForeignKey(d => d.TaskId)
                        .HasConstraintName("task_content_version_task_id_fkey");
                });

            modelBuilder.Entity<TaskView>(
                entity =>
                {
                    entity.ToTable("task_view");

                    entity.HasKey(e => e.Id);

                    entity.Property(e => e.Id).HasColumnName("id");
                    entity.Property(e => e.Name).HasColumnName("name")
                        .HasMaxLength(32);
                    entity.Property(e => e.TasksetId).HasColumnName("taskset_id");
                    entity.Property(e => e.Content).HasColumnName("content");
                    entity.Property(e => e.ContentVersionId).HasColumnName("content_version_id");
                    entity.Property(e => e.EditedTimestamp).HasColumnName("edited_timestamp");
                    entity.Property(e => e.Version).HasColumnName("version");

                    entity.HasOne(d => d.Task)
                        .WithOne()
                        .HasForeignKey<TaskView>(d => d.Id);
                    entity.HasOne(d => d.ContentVersion)
                        .WithOne()
                        .HasForeignKey<TaskView>(d => d.ContentVersionId);
                    entity.HasOne(d => d.Taskset)
                        .WithOne()
                        .HasForeignKey<TaskView>(d => d.TasksetId);
                });

            modelBuilder.Entity<Taskset>(
                entity =>
                {
                    entity.ToTable("taskset");

                    entity.HasKey(e => e.Id);

                    entity.HasIndex(e => new {e.CourseId, e.Year, e.Name})
                        .HasName("no_identical_tasksets")
                        .IsUnique();

                    entity.Property(e => e.Id)
                        .HasColumnName("id")
                        .UseNpgsqlIdentityAlwaysColumn();
                    entity.Property(e => e.CourseId).HasColumnName("course_id");
                    entity.Property(e => e.Name)
                        .HasColumnName("name")
                        .HasMaxLength(32);
                    entity.Property(e => e.Year).HasColumnName("year");

                    entity.HasOne(d => d.Course)
                        .WithMany(p => p.Tasksets)
                        .HasForeignKey(d => d.CourseId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("taskset_course_id_fkey");
                });

            modelBuilder.Entity<Upvote>(
                entity =>
                {
                    entity.ToTable("upvote");

                    entity.HasKey(e => new {e.SolutionId, e.Author})
                        .HasName("no_multiple_upvotes");

                    entity.Property(e => e.Author)
                        .IsRequired()
                        .HasColumnName("author")
                        .HasMaxLength(36);
                    entity.Property(e => e.SolutionId).HasColumnName("solution_id");

                    entity.HasOne(d => d.Solution)
                        .WithMany(p => p.Upvotes)
                        .HasForeignKey(d => d.SolutionId)
                        .HasConstraintName("upvote_solution_id_fkey");
                });
        }
    }
}