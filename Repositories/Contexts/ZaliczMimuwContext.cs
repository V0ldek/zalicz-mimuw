﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using Repositories.Entities;
using Repositories.Repositories;
using Repositories.StoredFunctions;
using Utilities.Assertions;
using Task = Repositories.Entities.Task;

namespace Repositories.Contexts
{
    using CreateTaskStoredFunctionParameters = StoredFunctionParameters<int, string, string, string>;
    using CreateSolutionStoredFunctionParameters = StoredFunctionParameters<int, string, string>;
    using CreateNegativeReviewStoredFunctionParameters = StoredFunctionParameters<int, string, string>;
    using CreateStoredFunctionResult = StoredFunctionResult<int, int>;
    using UpdateStoredFunctionParameters = StoredFunctionParameters<int, string, string>;
    using UpdateStoredFunctionResult = StoredFunctionResult<int>;

    public partial class ZaliczMimuwContext : DbContext, IZaliczMimuwRepository
    {
        public ZaliczMimuwContext(DbContextOptions<ZaliczMimuwContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<NegativeReviewView> NegativeReviewViews { get; set; }
        public virtual DbSet<Solution> Solutions { get; set; }
        public virtual DbSet<SolutionView> SolutionViews { get; set; }
        public virtual DbSet<Task> Tasks { get; set; }
        public virtual DbSet<TaskView> TaskViews { get; set; }
        public virtual DbSet<Taskset> Tasksets { get; set; }
        public virtual DbSet<Upvote> Upvotes { get; set; }

        public Task<int> SaveChangesAsync() => base.SaveChangesAsync();

        public async Task<(int taskId, int contentVersionId)> CreateTaskAsync(
            int tasksetId,
            string name,
            string content,
            string author)
        {
            var parameters = new CreateTaskStoredFunctionParameters(tasksetId, name, content, author);
            var result =
                await CallStoredFunction<CreateTaskStoredFunctionParameters, CreateStoredFunctionResult>(
                    "create_task",
                    parameters);

            return result.ToTuple();
        }

        public async Task<(int solutionId, int contentVersionId)> CreateSolutionAsync(
            int taskId,
            string content,
            string author)
        {
            var parameters = new CreateSolutionStoredFunctionParameters(taskId, content, author);
            var result =
                await CallStoredFunction<CreateSolutionStoredFunctionParameters, CreateStoredFunctionResult>(
                    "create_solution",
                    parameters);

            return result.ToTuple();
        }

        public async Task<(int negativeReviewId, int contentVersionId)> CreateNegativeReviewAsync(
            int solutionId,
            string content,
            string author)
        {
            var parameters = new CreateNegativeReviewStoredFunctionParameters(solutionId, content, author);
            var result =
                await CallStoredFunction<CreateNegativeReviewStoredFunctionParameters, CreateStoredFunctionResult>(
                    "create_negative_review",
                    parameters);

            return result.ToTuple();
        }

        public async Task<int> UpdateTaskContentVersionAsync(
            int originalContentVersionId,
            string content,
            string author)
        {
            var parameters = new UpdateStoredFunctionParameters(originalContentVersionId, content, author);
            var result =
                await CallStoredFunction<UpdateStoredFunctionParameters, UpdateStoredFunctionResult>(
                    "update_task_content_version",
                    parameters);

            return result.ToTuple();
        }

        public async Task<int> UpdateSolutionContentVersionAsync(
            int originalContentVersionId,
            string content,
            string author)
        {
            var parameters = new UpdateStoredFunctionParameters(originalContentVersionId, content, author);
            var result = await CallStoredFunction<UpdateStoredFunctionParameters, UpdateStoredFunctionResult>(
                "update_solution_content_version",
                parameters);

            return result.ToTuple();
        }

        public async Task<int> UpdateNegativeReviewContentVersionAsync(
            int originalContentVersionId,
            string content,
            string author)
        {
            var parameters = new UpdateStoredFunctionParameters(originalContentVersionId, content, author);
            var result = await CallStoredFunction<UpdateStoredFunctionParameters, UpdateStoredFunctionResult>(
                "update_negative_review_content_version",
                parameters);

            return result.ToTuple();
        }

        private Task<TResult> CallStoredFunction<TParameters, TResult>(string functionName, TParameters parameters)
            where TParameters : IStoredFunctionParameters
            where TResult : IStoredFunctionResult, new()
        {
            var storedFunction = new StoredFunction<TParameters, TResult>(functionName);
            var connection = Database.GetDbConnection() as NpgsqlConnection;
            Assert.IsNotNull(connection, nameof(connection));

            return storedFunction.ExecuteAsync(parameters, connection);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
            Assert.IsTrue(optionsBuilder.IsConfigured, "DbContext should be configured via dependency injection.");
    }
}