﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Repositories.Entities;

namespace Repositories.Repositories
{
    public interface IZaliczMimuwRepository
    {
        DbSet<Course> Courses { get; set; }

        DbSet<NegativeReviewView> NegativeReviewViews { get; set; }

        DbSet<SolutionView> SolutionViews { get; set; }

        DbSet<TaskView> TaskViews { get; set; }

        DbSet<Taskset> Tasksets { get; set; }

        DbSet<Upvote> Upvotes { get; set; }

        Task<int> SaveChangesAsync();

        Task<(int taskId, int contentVersionId)> CreateTaskAsync(
            int tasksetId,
            string name,
            string content,
            string author);

        Task<(int solutionId, int contentVersionId)> CreateSolutionAsync(int taskId, string content, string author);

        Task<(int negativeReviewId, int contentVersionId)> CreateNegativeReviewAsync(
            int solutionId,
            string content,
            string author);

        Task<int> UpdateTaskContentVersionAsync(int originalContentVersionId, string content, string author);

        Task<int> UpdateSolutionContentVersionAsync(int originalContentVersionId, string content, string author);

        Task<int> UpdateNegativeReviewContentVersionAsync(int originalContentVersionId, string content, string author);
    }
}