﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace Utilities.Assertions
{
    [Serializable]
    public class AssertionException : ApplicationException
    {
        private AssertionException(string message) : base(message)
        {
        }

        protected AssertionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        internal static AssertionException MakeGeneric([CallerMemberName] string caller = "") =>
            new AssertionException($"{caller} failed.");

        internal static AssertionException MakeValueFailure(
            string failingName,
            string failingValue,
            [CallerMemberName] string caller = "") =>
            new AssertionException($"{caller} failed for {failingName} = {failingValue}.");

        internal static AssertionException MakeCustomMessage(string message) => new AssertionException(message);
    }
}