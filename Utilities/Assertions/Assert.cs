﻿using JetBrains.Annotations;

namespace Utilities.Assertions
{
    public static class Assert
    {
        [ContractAnnotation("value: null => halt")]
        public static void IsNotNull<T>(T value)
        {
            if (value == null)
            {
                throw AssertionException.MakeGeneric();
            }
        }

        [ContractAnnotation("value: null => halt")]
        public static void IsNotNull<T>(T value, string name)
        {
            if (value == null)
            {
                throw AssertionException.MakeValueFailure(name, "null");
            }
        }

        [ContractAnnotation("predicate: false => halt")]
        public static void IsTrue(bool predicate, string errorMessage)
        {
            if (!predicate)
            {
                throw AssertionException.MakeCustomMessage(errorMessage);
            }
        }

        [ContractAnnotation("predicate: true => halt")]
        public static void IsFalse(bool predicate, string errorMessage)
        {
            if (predicate)
            {
                throw AssertionException.MakeCustomMessage(errorMessage);
            }
        }

        [ContractAnnotation("value: null => halt")]
        public static void IsNotNullOrEmpty(string value, string errorMessage)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw AssertionException.MakeCustomMessage(errorMessage);
            }
        }

        public static void IsNotZero(int value, string errorMessage)
        {
            if (value == 0)
            {
                throw AssertionException.MakeCustomMessage(errorMessage);
            }
        }
    }
}